package bloomnet;
import java.io.File;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.util.EntityUtils;

public class PostDeliveryData{
	
	private static final String apiKey = "mf$Sf3m!tkdsZkf5nP(7dnsD$1co9DGjNsf2niCn$0vUf(I9em";

	  public static void main(String[] args) throws Exception {
		///////////////////Save an image for use with data saving later on  
		MyHttpClient client = new MyHttpClient();
		CloseableHttpClient httpclient = client.getNewHttpClient(false);//Staging use ONLY, accepts ALL certifcates
		//CloseableHttpClient httpclient = client.getNewHttpClient(true);//Production use

	   /* HttpPost httppost = new HttpPost("https://108.166.23.95/DeliveryAPI/v1/SaveImage");
	    File file = new File("C:\\102423.png");
	    
	    HttpEntity httpEnt = MultipartEntityBuilder.create()
	    		.addPart("apiKey", new StringBody("bloomnettestkey", ContentType.TEXT_PLAIN))
	    		.addPart("file", new FileBody(file, ContentType.MULTIPART_FORM_DATA))
	    		.build();
	    httppost.setEntity(httpEnt);
	    
	    System.out.println("executing request " + httppost.getRequestLine());
	    
	    HttpResponse response = httpclient.execute(httppost);
	    HttpEntity resEntity = response.getEntity();

	    String savedImage = "";
	    if (resEntity != null) {
	      String JSON = EntityUtils.toString(resEntity);
	      
	      if(JSON.contains("successMessage")){
	    	  savedImage = JSON.split("\"imageName\":\"")[1].split("\"")[0];
	      }else{
	    	  savedImage = "no image saved";
	      }
	      System.out.println(JSON);
	      System.out.println(savedImage);
	      EntityUtils.consume(resEntity);
	    }*/
	    //////////////////Save delivery Data
	    HttpPost httppost = new HttpPost("https://delivery.bloomnet.net/DeliveryAPIBeta/v1/InsertOrUpdateDeliveryInfo");
	    for(int ii=100; ii<110; ++ii){
		    HttpEntity httpEnt = MultipartEntityBuilder.create()
		    		.addPart("apiKey", new StringBody(apiKey, ContentType.TEXT_PLAIN))
		    		.addPart("fulfiller", new StringBody("A2220000", ContentType.TEXT_PLAIN))
		    		.addPart("deliveryDate", new StringBody("07/27/2016", ContentType.TEXT_PLAIN))
		    		.addPart("orderNumberBMS", new StringBody("SPIDERMAN2099001"+String.valueOf(ii), ContentType.TEXT_PLAIN))
		    		.addPart("orderNumberOther", new StringBody("SPIDERMAN2099002"+String.valueOf(ii), ContentType.TEXT_PLAIN))
		    		.addPart("externalId", new StringBody("8DB7E0A9D4764E690D5413A5475FBE9AA", ContentType.TEXT_PLAIN))
		    		.addPart("deliveryCo", new StringBody("Route4Me", ContentType.TEXT_PLAIN))
		    		.addPart("deliveryImageURL", new StringBody("http://adb6def9928467589ebb-f540d5a8d53c2e76ad581b6e5c346ad6.r74.cf1.rackcdn.com/f140776feaf36b419a1a1975b5a302c8.jpg", ContentType.TEXT_PLAIN))
		    		.addPart("addressLine1", new StringBody("216 JAFFREY ST", ContentType.TEXT_PLAIN))
		    		.addPart("city", new StringBody("WEYMOUTH", ContentType.TEXT_PLAIN))
		    		.addPart("state", new StringBody("MA", ContentType.TEXT_PLAIN))
		    		.addPart("zip", new StringBody("02188", ContentType.TEXT_PLAIN))
		    		.addPart("country", new StringBody("US", ContentType.TEXT_PLAIN))
		    		.addPart("targetDeliveryDate", new StringBody("03/30/2016", ContentType.TEXT_PLAIN))
		    		.addPart("merchant", new StringBody("00800000", ContentType.TEXT_PLAIN))
		    		.addPart("lat", new StringBody("46.2447", ContentType.TEXT_PLAIN))
		    		.addPart("lon", new StringBody("105.2675", ContentType.TEXT_PLAIN))
		    		.build();
		    httppost.setEntity(httpEnt);
		    
		    System.out.println("executing request " + httppost.getRequestLine());
		    HttpResponse response = httpclient.execute(httppost);
		    HttpEntity resEntity = response.getEntity();
		    System.out.println(EntityUtils.toString(resEntity));
		    EntityUtils.consume(resEntity);
	    }
	    
	    ///////////////////Get Delivery Data
	    
	  HttpGet httpget = new HttpGet("http://delivery.bloomnet.net/DeliveryAPI/v1/RetrieveDeliveryInfoByOrderNumber?apiKey="+apiKey+"&orderNumber=9104436");
	    
	    
	    System.out.println("executing request " + httpget.getRequestLine());
	    response = httpclient.execute(httpget);
	    resEntity = response.getEntity();
	    System.out.println(EntityUtils.toString(resEntity));
	    EntityUtils.consume(resEntity); 
	    httpclient.close();
	  }
}
