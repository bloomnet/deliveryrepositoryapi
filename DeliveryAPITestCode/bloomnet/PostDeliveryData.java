package test;
import java.io.File;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.util.EntityUtils;

public class PostDeliveryData{	

	  public static void main(String[] args){
		///////////////////Save an image for use with data saving later on  
		MyHttpClient client = new MyHttpClient();
		CloseableHttpClient httpclient = client.getNewHttpClient(false);//Staging use ONLY, accepts ALL certifcates
		//CloseableHttpClient httpclient = client.getNewHttpClient(true);//Production use

	   /* HttpPost httppost = new HttpPost("https://108.166.23.95/DeliveryAPI/v1/SaveImage");
	    File file = new File("C:\\102423.png");
	    
	    HttpEntity httpEnt = MultipartEntityBuilder.create()
	    		.addPart("apiKey", new StringBody("bloomnettestkey", ContentType.TEXT_PLAIN))
	    		.addPart("file", new FileBody(file, ContentType.MULTIPART_FORM_DATA))
	    		.build();
	    httppost.setEntity(httpEnt);
	    
	    System.out.println("executing request " + httppost.getRequestLine());
	    
	    HttpResponse response = httpclient.execute(httppost);
	    HttpEntity resEntity = response.getEntity();

	    String savedImage = "";
	    if (resEntity != null) {
	      String JSON = EntityUtils.toString(resEntity);
	      
	      if(JSON.contains("successMessage")){
	    	  savedImage = JSON.split("\"imageName\":\"")[1].split("\"")[0];
	      }else{
	    	  savedImage = "no image saved";
	      }
	      System.out.println(JSON);
	      System.out.println(savedImage);
	      EntityUtils.consume(resEntity);
	    }
	    //////////////////Save delivery Data
	    httppost = new HttpPost("https://108.166.23.95/DeliveryAPI/v1/InsertOrUpdateDeliveryInfo");
	    httpEnt = MultipartEntityBuilder.create()
	    		.addPart("apiKey", new StringBody("bloomnettestkey", ContentType.TEXT_PLAIN))
	    		.addPart("fulfiller", new StringBody("B5230000", ContentType.TEXT_PLAIN))
	    		.addPart("deliveryDate", new StringBody("01/08/2016", ContentType.TEXT_PLAIN))
	    		.addPart("orderNumberBLK", new StringBody("72309183746", ContentType.TEXT_PLAIN))
	    		.addPart("deliveryTime", new StringBody("12:00PM", ContentType.TEXT_PLAIN))
	    		.addPart("leftAt", new StringBody("Side Door", ContentType.TEXT_PLAIN))
	    		.addPart("signedBy", new StringBody("Mark Silver", ContentType.TEXT_PLAIN))
	    		.addPart("signatureImageName", new StringBody(savedImage, ContentType.TEXT_PLAIN))
	    		.addPart("deliveryImageName", new StringBody(savedImage, ContentType.TEXT_PLAIN))
	    		.addPart("addressLine1", new StringBody("39 Walnut St", ContentType.TEXT_PLAIN))
	    		.addPart("city", new StringBody("West Hempstead", ContentType.TEXT_PLAIN))
	    		.addPart("state", new StringBody("NY", ContentType.TEXT_PLAIN))
	    		.addPart("zip", new StringBody("11552", ContentType.TEXT_PLAIN))
	    		.addPart("country", new StringBody("USA", ContentType.TEXT_PLAIN))
	    		.addPart("targetDeliveryDate", new StringBody("01/08/2016", ContentType.TEXT_PLAIN))
	    		.addPart("targetDeliveryTime", new StringBody("12:00PM", ContentType.TEXT_PLAIN))
	    		.addPart("driverName", new StringBody("Pablo", ContentType.TEXT_PLAIN))
	    		.addPart("merchant", new StringBody("M1770000", ContentType.TEXT_PLAIN))
	    		.addPart("deliveryNotes", new StringBody("Done Good!", ContentType.TEXT_PLAIN))
	    		.build();
	    httppost.setEntity(httpEnt);
	    
	    System.out.println("executing request " + httppost.getRequestLine());
	    response = httpclient.execute(httppost);
	    resEntity = response.getEntity();
	    System.out.println(EntityUtils.toString(resEntity));
	    EntityUtils.consume(resEntity);
	    
	    ///////////////////Get Delivery Data*/
	    
	    HttpGet httpget = new HttpGet("https://108.166.23.95/DeliveryAPI/v1/RetrieveDeliveryInfoByOrderNumber?apiKey=bloomnettestkey&orderNumber=72309183746");
	    
	    
	    System.out.println("executing request " + httpget.getRequestLine());
	    response = httpclient.execute(httpget);
	    resEntity = response.getEntity();
	    System.out.println(EntityUtils.toString(resEntity));
	    EntityUtils.consume(resEntity);
	    httpclient.close();
	    
	    HttpGet httpget = new HttpGet("https://108.166.23.95/DeliveryAPI/v1/RetrieveAddressesByShopCodeAndDeliveryDate?apiKey=bloomnettestkey&shopCode=B5230000&deliveryDate=01/08/2016");
	    
	    System.out.println("executing request " + httpget.getRequestLine());
	    response = httpclient.execute(httpget);
	    resEntity = response.getEntity();
	    System.out.println(EntityUtils.toString(resEntity));
	    EntityUtils.consume(resEntity);
	    httpclient.close();
	    
	  }
}
