<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="net.bloomnet.deliveryapi.dbobjects.Delivery" %>
<html> 
	<body> 
		  <% 
		  String errorMessage = String.valueOf(request.getAttribute("errorMessage"));
		  if(errorMessage != null && !errorMessage.trim().equals("") && !errorMessage.trim().equalsIgnoreCase("null")){
		  %>
		  <%=errorMessage %>
		  <%}else{
			  Delivery deliveryData = (Delivery)request.getAttribute("deliveryData");
			  %>
			  
			  <table>
			  	<%if(deliveryData.getOrderNumberAtlas() != null){ %>
			  		<tr>
			  			<td>
			  			Atlas Order Number: <%=deliveryData.getOrderNumberAtlas() %>
			  			</td>
			  		</tr>
			  	<%} %>
			  	<%if(deliveryData.getOrderNumberBLK() != null){ %>
			  		<tr>
			  			<td>
			  			Bloomlink Order Number: <%=deliveryData.getOrderNumberBLK() %>
			  			</td>
			  		</tr>
			  	<%} %>
			  	<%if(deliveryData.getOrderNumberBMS() != null){ %>
			  		<tr>
			  			<td>
			  			BMS Order Number: <%=deliveryData.getOrderNumberBMS() %>
			  			</td>
			  		</tr>
			  	<%} %>
			  	<%if(deliveryData.getOrderNumberDeliv() != null){ %>
			  		<tr>
			  			<td>
			  			Delivery Order Number: <%=deliveryData.getOrderNumberDeliv() %>
			  			</td>
			  		</tr>
			  	<%} %>
			  	<%if(deliveryData.getOrderNumberOnFleet() != null){ %>
			  		<tr>
			  			<td>
			  			On Fleet Order Number: <%=deliveryData.getOrderNumberOnFleet() %>
			  			</td>
			  		</tr>
			  	<%} %>
			  	<%if(deliveryData.getOrderNumberOther() != null){ %>
			  		<tr>
			  			<td>
			  			 Other Order Number: <%=deliveryData.getOrderNumberOther() %>
			  			</td>
			  		</tr>
			  	<%} %>
			  	<%if(deliveryData.getDeliveringShopCode() != null){ %>
			  		<tr>
			  			<td>
			  			Delivering Shop Code: <%=deliveryData.getDeliveringShopCode() %>
			  			</td>
			  		</tr>
			  	<%} %>
			  	<%if(deliveryData.getDeliveryDate() != null){ %>
			  		<tr>
			  			<td>
			  			Delivery Date: <%=deliveryData.getDeliveryDate() %>
			  			</td>
			  		</tr>
			  	<%} %>
			  	<%if(deliveryData.getDeliveryTime() != null){ %>
			  		<tr>
			  			<td>
			  			Atlas Order Number: <%=deliveryData.getDeliveryTime() %>
			  			</td>
			  		</tr>
			  	<%} %>
			  	<%if(deliveryData.getSignedBy() != null){ %>
			  		<tr>
			  			<td>
			  			Signed By: <%=deliveryData.getSignedBy() %>
			  			</td>
			  		</tr>
			  	<%} %>
			  	<%if(deliveryData.getLeftAt() != null){ %>
			  		<tr>
			  			<td>
			  			Left At: <%=deliveryData.getLeftAt() %>
			  			</td>
			  		</tr>
			  	<%} %>
			  	<%if(deliveryData.getSignatureImageName() != null){ %>
			  		<tr>
			  			<td>
			  			<img src="/images/<%=deliveryData.getSignatureImageName() %>" />
			  			</td>
			  		</tr>
			  	<%} %>
			  	<%if(deliveryData.getSignatureImageURL() != null){ %>
			  		<tr>
			  			<td>
			  			<img src="<%=deliveryData.getSignatureImageURL() %>" />
			  			</td>
			  		</tr>
			  	<%} %>
			  </table>
			<%} %>
 	</body>
</html>
