<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="net.bloomnet.deliveryapi.entity.Delivery" %>
<%@ page import="net.bloomnet.deliveryapi.entity.Address" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.Map" %>
<%@ page import="java.util.HashMap" %>

<html> 
	<head>
	<title>Delivery Addresses Map</title>
	<script src = "https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&key=AIzaSyBdJrLFts-UP4Nw1fqqsdlDYOmzoqjtTpc"></script>
	<script type="text/javascript">
		 var previousInfoWindow;
		 var geoCoder = new google.maps.Geocoder();
		 var basicMap;
		 const queryString = window.location.search;
		 const urlParams = new URLSearchParams(queryString);
		 const apiKey = urlParams.get('apiKey');
		 
		 function sleep(ms) {
			  return new Promise(resolve => setTimeout(resolve, ms));
		}
		 
		 function initialize() {
				
				geoCoder.geocode( { 'address': "Lebanon, Kansas"}, function(results, status) {
				if (status == google.maps.GeocoderStatus.OK) {
					var lat = results[0]['geometry']['location'].lat();
					var lng = results[0]['geometry']['location'].lng();
					var centerLatLng = new google.maps.LatLng(lat,lng);
					var mapOptions = {
						zoom: 5,
						center: centerLatLng,
						mapTypeId: google.maps.MapTypeId.ROADMAP
					  };
					  basicMap = new google.maps.Map(document.getElementById('googleMap'),
						  mapOptions);
					  
					  addMarkers();
				}	
			 
								});
			}
		 
		 function replaceAll(str, find, replacement) {
			    return str.replace(new RegExp(find, 'g'), replacement);
			}
		 
		  async function addMarkers() {
			  
			  <% 
			  Map<String,String> addressesSeen = new HashMap<String,String>();
			  StringBuilder addressLinesString = new StringBuilder();
			  StringBuilder addressLinesLatLonString = new StringBuilder();
			  @SuppressWarnings("unchecked")
			  List<Delivery> deliveryData = (List<Delivery>) request.getAttribute("deliveryData");
			  if(deliveryData != null){
			  	for(int ii=0; ii < deliveryData.size(); ii++){
			  		try{
			  			Delivery delivery = deliveryData.get(ii); 
			  			String deliveryNotes = delivery.getDeliveryNotes(); 
			  			if(deliveryNotes == null)
			  				deliveryNotes=""; 
			  			else deliveryNotes = deliveryNotes.replaceAll("\"","'"); 
			  			if(delivery.getSignatureImageURL() != null && !delivery.getSignatureImageURL().equals(""))
			  				delivery.setSignatureImageURL("click here"); 
			  			if(delivery.getDeliveryImageURL() != null && !delivery.getDeliveryImageURL().equals("")) 
			  				delivery.setDeliveryImageURL("click here"); String signedBy = delivery.getSignedBy(); 
			  			if(signedBy == null)
			  				signedBy=""; 
			  			else 
			  				deliveryNotes = deliveryNotes.replaceAll("\"","'");
			  			Address address = delivery.getAddress();
			  			if((address.getLat() == null || address.getLat().equals("")) && (address.getLon() == null || address.getLon().equals(""))){
			  				String addressLineItem = address.getAddressLine1().replaceAll("\"","'") + "," + address.getCity() + "," + address.getState() + "," + address.getZip() +"---BMSOrderNumber: " + delivery.getOrderNumberBMS()+"<br />AtlasOrderNumber: "+ delivery.getOrderNumberAtlas() +"<br />SendingShopCode: "+ delivery.getMerchant() +"<br />FulfillingShopCode: "+ delivery.getFulfiller()+"<br />DeliveryTime: "+ delivery.getDeliveryTime() +"<br />SignedBy: "+ signedBy.replaceAll("\"","'") +"<br />DriverName: "+ delivery.getDriverName() +"<br />SignatureURL: <a href='"+request.getContextPath()+"/v1/RetrieveSignatureImage?apiKey="+request.getParameter("apiKey")+"&orderNumber="+delivery.getOrderNumberBMS()+"'>"+ delivery.getSignatureImageURL() +"</a><br />DeliveryImageURL: <a href='"+request.getContextPath()+"/v1/RetrieveDeliveryImage?apiKey="+request.getParameter("apiKey")+"&orderNumber="+delivery.getOrderNumberBMS()+"'>"+ delivery.getDeliveryImageURL() +"</a><br />DeliveryCo: "+ delivery.getDeliveryCo() +"<br />ExternalID: "+ delivery.getExternalId() +"<br />TargetDeliveryDate: "+ delivery.getTargetDeliveryDate() + "<br />DeliveryDate: "+ delivery.getDeliveryDate() +"<br />EstimatedDeliveryDate: "+ delivery.getEstimatedDeliveryDate() +"<br />EstimatedDeliveryTime: "+ delivery.getEstimatedDeliveryTime() +"<br />DeliveryCoID: "+ delivery.getDeliveryCoID() +"<br />DeliveryCoTripID: "+ delivery.getDeliveryCoTripID() +"<br />Status: "+ delivery.getStatus() +"<br />DeliveryNotes: "+ deliveryNotes.replaceAll("\"","").replaceAll("\\&","and").replaceAll("'","").replaceAll("\\+","").replaceAll("\\%","").replaceAll("\\#", "number").replaceAll("\\>","").replaceAll("/","").replaceAll("\r","").replaceAll("\n","") +"///";
			  				if(addressesSeen.get(addressLineItem) == null){
			  					addressLinesString.append(addressLineItem);
			  					addressesSeen.put(addressLineItem,"1");
			  				} 
			  			}else if(address.getLat() != null && !address.getLat().equals("") && address.getLon() != null && !address.getLon().equals("")){ 
			  				String addressLineLatLonItem = address.getAddressLine1().replaceAll("\"","'") + "," + address.getCity() + "," + address.getState() + "," + address.getZip() + "," + address.getLat() + "," + address.getLon() +"---BMSOrderNumber: " + delivery.getOrderNumberBMS()+"<br />AtlasOrderNumber: "+ delivery.getOrderNumberAtlas() +"<br />SendingShopCode: "+ delivery.getMerchant() +"<br />FulfillingShopCode: "+ delivery.getFulfiller()+"<br />DeliveryTime: "+ delivery.getDeliveryTime() +"<br />SignedBy: "+ signedBy.replaceAll("\"","'") +"<br />DriverName: "+ delivery.getDriverName() +"<br />SignatureURL: <a href='"+request.getContextPath()+"/v1/RetrieveSignatureImage?apiKey="+request.getParameter("apiKey")+"&orderNumber="+delivery.getOrderNumberBMS()+"'>"+ delivery.getSignatureImageURL() +"</a><br />DeliveryImageURL: <a href='"+request.getContextPath()+"/v1/RetrieveDeliveryImage?apiKey="+request.getParameter("apiKey")+"&orderNumber="+delivery.getOrderNumberBMS()+"'>"+ delivery.getDeliveryImageURL() +"</a><br />DeliveryCo: "+ delivery.getDeliveryCo() +"<br />ExternalID: "+ delivery.getExternalId() +"<br />TargetDeliveryDate: "+ delivery.getTargetDeliveryDate() + "<br />DeliveryDate: "+ delivery.getDeliveryDate() +"<br />EstimatedDeliveryDate: "+ delivery.getEstimatedDeliveryDate() +"<br />EstimatedDeliveryTime: "+ delivery.getEstimatedDeliveryTime() +"<br />DeliveryCoID: "+ delivery.getDeliveryCoID() +"<br />DeliveryCoTripID: "+ delivery.getDeliveryCoTripID() +"<br />Status: "+ delivery.getStatus() +"<br />DeliveryNotes: "+ deliveryNotes.replaceAll("\"","").replaceAll("\\&","and").replaceAll("'","").replaceAll("\\+","").replaceAll("\\%","").replaceAll("\\#", "number").replaceAll("\\>","").replaceAll("/","").replaceAll("\r","").replaceAll("\n","") +"///"; 
			  				if(addressesSeen.get(addressLineLatLonItem) == null){
			  					addressLinesLatLonString.append(addressLineLatLonItem); 
			  					addressesSeen.put(addressLineLatLonItem,"1");
			  				} 
			  			}
			  		}catch(Exception ee){}
			  	}
			  }
			  %>
			 
			  geoCoder = new google.maps.Geocoder();
			  var addressLinesString = "<%=addressLinesString.toString() %>";
			  var addressLines = addressLinesString.split("///");
			  var addressLinesLatLonString = "<%=addressLinesLatLonString.toString() %>";
			  var addressLinesLatLon = addressLinesLatLonString.split("///");
			  if(addressLinesLatLon.length == 1){
				  addMarkerByLatLon(addressLinesLatLon[0],);
			  }else if(addressLinesLatLon.length > 1){
				  for(var ii=0; ii<addressLinesLatLon.length; ii++){
					  addMarkerByLatLon(addressLinesLatLon[ii]);
				  }
			  }
			  if(addressLines.length == 1){
				  addMarker(addressLines[0]);
			  }else if(addressLines.length > 49){
				  for(var ii=0; ii<addressLines.length; ii++){
					try{
						if(ii !== 0 && ii%50 === 0){
							for(var xx=0; xx<50; ++xx){
								addMarker(addressLines[ii-xx]);
							}
						}else if(ii === 0){
							addMarker(addressLines[ii]);
							addMarker(addressLines[ii+1]);
							addMarker(addressLines[ii+2]);
							addMarker(addressLines[ii+3]);
							addMarker(addressLines[ii+4]);
							addMarker(addressLines[ii+5]);
							addMarker(addressLines[ii+6]);
							addMarker(addressLines[ii+7]);
							addMarker(addressLines[ii+8]);
							addMarker(addressLines[ii+9]);
						}else if(ii == addressLines.length - 1){
							for(var xx = (ii-(ii%50))+1; xx<addressLines.length; ++xx){
								addMarker(addressLines[xx]);
							}
						}
						
					}catch(err){
						continue;
					}
				  }	
			  }else{
				  for(var ii=0; ii<addressLines.length; ii++){
					  	var addressLine = addressLines[ii];
						try{
							addMarker(addressLine);
								
						}catch(err){
							continue;
						}
				  }
			  }
		 }
	
			async function addMarker(addressLine){
				var splitData = addressLine.split("---");
				var splitItems = splitData[0].split(",");
				var addressLine1 = "";
				var city = "";
				var state = "";
				var zip = "";
				var status = "";
				addressLine1 = splitItems[0];
				city = splitItems[1];
				state = splitItems[2];
				zip = splitItems[3];
				status = splitData[1].split("Status: ")[1].split("<br />")[0];
				
				var latLng;
				
				var contentString = '<div style="width:400px; overflow:hidden;">'+
				 '<div id="bodyContent">'+
				 '<div style="font-weight:bold; color:#719d48;">'+addressLine1+'</div>'+
				 addressLine1+'<br />'+
				 city+', '+state+', '+zip+'<br />'+
				 replaceAll(splitData[1],"null","")+
				 '</div>';
				 
				 var infowindow = new google.maps.InfoWindow({
				  content: contentString
				  });
				
				var req = new XMLHttpRequest();
				var url="RetreiveGeoCoding?address="+splitData[0]+"&apiKey="+apiKey;
				var results = "";
				
				req.open("GET", url, true);
				req.responseType = "json";
				req.send();
				req.onload = function() {
					  results = req.response;
					  var lat = results.results[0]['geometry']['location']['lat'];
					  var lng = results.results[0]['geometry']['location']['lng'];
					  latLng = new google.maps.LatLng(lat,lng);
					  var deliveryMarker;
					  if(status === "Delivered"){
						  deliveryMarker = new google.maps.Marker({
							  map: basicMap,
							  position: latLng,
							  title: (addressLine1),
							  icon: {
							      url: "http://maps.google.com/mapfiles/ms/icons/green-dot.png"
							  }
						  });
						  
					  }else if(status === "OutForDelivery"){
						  deliveryMarker = new google.maps.Marker({
							  map: basicMap,
							  position: latLng,
							  title: (addressLine1),
							  icon: {
							      url: "http://maps.google.com/mapfiles/ms/icons/yellow-dot.png"
							  }
						  });
						  
					  }else{
						  deliveryMarker = new google.maps.Marker({
							  map: basicMap,
							  position: latLng,
							  title: (addressLine1),
							  icon: {
							      url: "http://maps.google.com/mapfiles/ms/icons/red-dot.png"
							  }
						  });
						  
					  }
						google.maps.event.addListener(deliveryMarker, 'click', function() {
							infowindow.open(basicMap,deliveryMarker);
													
							if(previousInfoWindow != null){	
								previousInfoWindow.close();
							}
							previousInfoWindow = infowindow;
							});
						
						
						req = new XMLHttpRequest();
						url="UpdateAddress?addressLine1="+addressLine1+"&city="+city+"&state="+state+"&zip="+zip+"&lat="+lat+"&lon="+lng+"&apiKey="+apiKey;
						req.open("GET", url, true);
						req.send();

				};			 
							
			}
			
			async function addMarkerByLatLon(addressLine){
				var splitData = addressLine.split("---");
				var splitItems = splitData[0].split(",");
				var addressLine1 = "";
				var city = "";
				var state = "";
				var zip = "";
				var lat = "";
				var lon = "";
				var status = "";
				
				addressLine1 = splitItems[0];
				city = splitItems[1];
				state = splitItems[2];
				zip = splitItems[3];
				lat = splitItems[4];
				lon = splitItems[5];
				status = splitData[1].split("Status: ")[1].split("<br />")[0];

				 var contentString = '<div style="width:400px; overflow:hidden;">'+
				  '<div id="bodyContent">'+
				  '<div style="font-weight:bold; color:#719d48;">'+addressLine1+'</div>'+
				  addressLine1+'<br />'+
				  city+', '+state+', '+zip+'<br />'+
				  replaceAll(splitData[1],"null","")+
				  '</div>';
				  
				  var infowindow = new google.maps.InfoWindow({
					  content: contentString
					  });
					var latLng = new google.maps.LatLng(lat,lon);
					var deliveryMarker;
					  if(status === "Delivered"){
						  deliveryMarker = new google.maps.Marker({
							  map: basicMap,
							  position: latLng,
							  title: (addressLine1),
							  icon: {
							      url: "http://maps.google.com/mapfiles/ms/icons/green-dot.png"
							  }
						  });
						  
					  }else if(status === "OutForDelivery"){
						  deliveryMarker = new google.maps.Marker({
							  map: basicMap,
							  position: latLng,
							  title: (addressLine1),
							  icon: {
							      url: "http://maps.google.com/mapfiles/ms/icons/yellow-dot.png"
							  }
						  });
						  
					  }else{
						  deliveryMarker = new google.maps.Marker({
							  map: basicMap,
							  position: latLng,
							  title: (addressLine1),
							  icon: {
							      url: "http://maps.google.com/mapfiles/ms/icons/red-dot.png"
							  }
						  });
						  
					  }
					google.maps.event.addListener(deliveryMarker, 'click', function() {
						infowindow.open(basicMap,deliveryMarker);
						if(previousInfoWindow != null){	
							previousInfoWindow.close();
						}
						previousInfoWindow = infowindow;
						});
					
							
				}

	 
	</script>
	
	</head>

	<body onload="initialize();"> 
		  <% 
		  String errorMessage = String.valueOf(request.getAttribute("errorMessage"));
		  //if there is an error message, show that, otherwise show the data
		  if(errorMessage != null && !errorMessage.trim().equals("") && !errorMessage.trim().equalsIgnoreCase("null")){ %>
		  	<%=errorMessage %>
		  <%}else{ %>
			  
			  <table>
			  	<tr><td> 
			  		<div id="googleMap" style="width:99%;height:98%;margin:0;padding:0;position:absolute;"></div> 
			  	</td></tr>
			  		
			  </table>
			 

			  <%} %>
 	</body>
</html>