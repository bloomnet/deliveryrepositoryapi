package net.bloomnet.deliveryapi.dao;

import java.util.List;

import net.bloomnet.deliveryapi.entity.Address;
import net.bloomnet.deliveryapi.entity.Delivery;
import net.bloomnet.deliveryapi.entity.DeliveryHistory;
import net.bloomnet.deliveryapi.entity.User;
import net.bloomnet.deliveryapi.entity.Webhooks;

public interface DeliveryAPIDAO {

	public static final String USER_COLLECTION = "user";
	public static final String DELIVERY_COLLECTION = "delivery_data";
	public static final String DELIVERY_HISTORY_COLLECTION = "delivery_history";
	public static final String ADDRESS_COLLECTION = "address";
	public static final String WEBHOOKS_COLLECTION = "webhooks";
	
	/* (non-Javadoc)
	 * Insert a new record. If the collection does not already exist in MongoDB, it will be craeted.
	 */
	public void addDelivery(Delivery delivery);
	
	/* (non-Javadoc)
	 * Update an existing MongoDB Document. This will overwrite any existing fields, drop any fields not provided, 
	 * and add any new fields that were not in the original document
	 */
	public void updateDelivery(Delivery delivery);
	
	public void addDeliveryHistory(DeliveryHistory delivery);
	
	public void updateAddress(Address address);
	
	/* (non-Javadoc)
	 * Returns a list of Delivery objects where the order number matches the provided order number.
	 */
	public List<Delivery> getDeliveriesByOrderNumber(String orderNumber);
	
	/* (non-Javadoc)
	 * Returns a list of Delivery History objects where the order number matches the provided order number.
	 */
	public List<DeliveryHistory> getDeliveriesHistoryByOrderNumber(String orderNumber);
	
	/* (non-Javadoc)
	 * Returns a list of User objects where the api key matches the provided api key.
	 */
	public List<User> getUsersByAPIKey(String apiKey);
	
	public void addAddress(Address address);
	
	public List<Address> getAddress(String addressLine1, String addressLine2, String city, String state, String zip, String country);
	
	public List<Delivery> getDeliveriesByMerchant(String merchant);
	
	public List<Delivery> getDeliveriesByFulfiller(String fulfiller);
	
	public List<Delivery> getDeliveriesByDateRange (String startD, String endD);
	
	public List<Delivery> getDeliveriesByExternalId (String externalId, String deliverCo);
	
	public List<Delivery> getDeliveriesByFulfillerAndTargetDeliveryDate (String fulfiller, String deliveryDate);
	
	public List<Delivery> getDeliveriesByTargetDeliveryDate (String deliveryDate);
	
	public List<Delivery> getDeliveriesByMerchantAndTargetDeliveryDate(String merchant, String deliveryDate);
	
	public List<Delivery> getDeliveriesByOrderNumbers(String orderNumbers);
	
	public List<Delivery> getDeliveriesByExternalIDs(String orderNumbers, String deliveryCo);

	public List<Delivery> getDeliveriesByDeliveryImageAndCreatedDate(String startDate, String endDate);
	
	public void addWebhooks(Webhooks webhooks);
	
	public void updateWebhooks(Webhooks webhooks);
	
	public List<Webhooks> getWebhooksByShopCode(String shopCode);
}