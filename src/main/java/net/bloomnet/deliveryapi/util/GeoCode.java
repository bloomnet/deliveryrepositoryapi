package net.bloomnet.deliveryapi.util;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

public class GeoCode {
	
	public String geoCode(String address){
		
		String urlA = "https://maps.googleapis.com/maps/api/geocode/json?address=";
		String urlB = "&inputtype=textquery&key=AIzaSyBdJrLFts-UP4Nw1fqqsdlDYOmzoqjtTpc";
		
		
		try {

          URL url = new URL((urlA + address + urlB).replaceAll(" ", "%20").replaceAll("\\+", "%20"));
          URLConnection con = url.openConnection();
          BufferedReader br = new BufferedReader(
                                new InputStreamReader(
                                con.getInputStream()));
          String inputLine;
          String response = "";

          while ((inputLine = br.readLine()) != null) 
              response += inputLine + " ";
          br.close();
          
          return response;
 		
		
		}catch(Exception ee){
			System.out.println("error: " + ee.getMessage()); 
			ee.printStackTrace();
			return null;
		}
	}
	
}
