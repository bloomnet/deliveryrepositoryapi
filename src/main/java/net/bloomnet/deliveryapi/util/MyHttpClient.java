package net.bloomnet.deliveryapi.util;

import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.HttpClientConnectionManager;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.BasicHttpClientConnectionManager;

public class MyHttpClient{ 
	
	public CloseableHttpClient getNewHttpClient(String url) throws NoSuchAlgorithmException, KeyManagementException, KeyStoreException {
		
		TrustManager[] trustAllCerts = new TrustManager[]{
    		    new X509TrustManager() {
    		        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
    		            return null;
    		        }
    		        public void checkClientTrusted(
    		            java.security.cert.X509Certificate[] certs, String authType) {
    		        }
    		        public void checkServerTrusted(
    		            java.security.cert.X509Certificate[] certs, String authType) {
    		        }
    		    }
    		};

		SSLContext context = SSLContext.getInstance("TLSv1.2");
		context.init(null, trustAllCerts, new java.security.SecureRandom());
	    HttpsURLConnection.setDefaultSSLSocketFactory(context.getSocketFactory());
		HttpClientBuilder builder = HttpClientBuilder.create();
	    SSLConnectionSocketFactory sslConnectionFactory = new SSLConnectionSocketFactory(context, new NoopHostnameVerifier());
	    builder.setSSLSocketFactory(sslConnectionFactory);

	    Registry<ConnectionSocketFactory> registry = RegistryBuilder.<ConnectionSocketFactory>create()
	            .register("https", sslConnectionFactory)
	            .build();

	    HttpClientConnectionManager ccm = new BasicHttpClientConnectionManager(registry);
	    builder.setConnectionManager(ccm);
	    
	    /*SSLSocketFactory ssf = context.getSocketFactory();
		try {
			SSLSocket socket = (SSLSocket) ssf.createSocket(
			        url, 443);
			 socket.startHandshake();
			 X509Certificate[] peerCertificates = (X509Certificate[]) socket
				        .getSession().getPeerCertificates();
			 TrustManagerFactory tmf = TrustManagerFactory
				        .getInstance(TrustManagerFactory.getDefaultAlgorithm());
				tmf.init((KeyStore) null);
				X509TrustManager tm = (X509TrustManager) tmf.getTrustManagers()[0];
				tm.checkServerTrusted(peerCertificates, "RSA");
		} catch (IOException e) {
			e.printStackTrace();
		} catch (CertificateException e) {
			e.printStackTrace();
		} */
		
	    
	    return builder.build();
		    
	}
}

