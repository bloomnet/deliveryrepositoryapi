package net.bloomnet.deliveryapi.controller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Random;

import net.bloomnet.deliveryapi.dao.DeliveryAPIDAO;
import net.bloomnet.deliveryapi.entity.Address;
import net.bloomnet.deliveryapi.entity.Delivery;
import net.bloomnet.deliveryapi.entity.DeliveryHistory;
import net.bloomnet.deliveryapi.entity.User;
import net.bloomnet.deliveryapi.entity.Webhooks;
import net.bloomnet.deliveryapi.responseobject.JSONInsertResponse;
import net.bloomnet.deliveryapi.responseobject.JSONRetrievalResponse;
import net.bloomnet.deliveryapi.util.CopyDeliveryToHistory;
import net.bloomnet.deliveryapi.util.GeoCode;
import net.bloomnet.deliveryapi.util.MyHttpClient;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.Base64Utils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/* (non-Javadoc)
 * This is the main controller for the entire application
 */
@Controller 
@EnableWebMvc
public class DeliveryAPIController {  
	
    private static final String UNAUTHENTICATED = "Your request was unauthorized. Please try again with proper authorization.";
	
    @Autowired
	private DeliveryAPIDAO dao;
    
    private List<String> usersList = new ArrayList<String>();
    
    Properties properties = new Properties();
	InputStream input = null;
	
	public DeliveryAPIController(){
		usersList.add("00800000");
	}
	
	@RequestMapping(value = "/v1/InsertOrUpdateDeliveryInfoInBulk")
	@Scope("request")
	public @ResponseBody JSONInsertResponse insertOrUpdateDeliveryInfoInBulk(
			@RequestParam(value="apiKey", required=false) String apiKey,
			@RequestParam(value="jsonArray", required=false) String jsonString){
		
		Map<String, String> userDetails = authorize(apiKey);
		JSONInsertResponse json = new JSONInsertResponse();
		
		if(userDetails != null && userDetails.size() == 2){
			
			ObjectMapper mapper = new ObjectMapper();
			List<Delivery> deliveries = null;
			try {
				deliveries = mapper.readValue(jsonString, new TypeReference<ArrayList<Delivery>>(){});
			} catch (JsonParseException e) {
				json.setErrorMessage("Malformed JSON. Please check all submitted fields. If all fields are correct and this error persists, please contact tech support.");
				e.printStackTrace();
				return json;
			} catch (JsonMappingException e) {
				json.setErrorMessage("Malformed JSON. Please check all submitted fields. If all fields are correct and this error persists, please contact tech support.");
				e.printStackTrace();
				return json;
			} catch (IOException e) {
				json.setErrorMessage("Malformed JSON. Please check all submitted fields. If all fields are correct and this error persists, please contact tech support.");
				e.printStackTrace();
				return json;
			}
			
			if(deliveries != null){
				if(deliveries.size() > 250){
					json.setErrorMessage("InsertOrUpdateDeliveryInfoInBulk only supports up to 250 transactions at once. Please try again with 250 or less total transactions.");
					return json;
				}
				String errorMessage = "Errors:";
				for(Delivery delivery : deliveries){
					if(delivery.getAddress() == null){
						errorMessage += " " + delivery.getOrderNumberBMS() +": No customer address was provided;";
						continue;
					}
					if(delivery.getShopAddress() == null){
						errorMessage += " " + delivery.getOrderNumberBMS() +": No fulfilling shop address was provided;";
						continue;
					}
					JSONInsertResponse response = insertOrUpdateDeliveryInfo(
							apiKey,
							delivery.getFulfiller(), 
							delivery.getDeliveryDate(), 
							delivery.getOrderNumberBLK(), 
							delivery.getOrderNumberAtlas(), 
							delivery.getOrderNumberBMS(), 
							delivery.getExternalId(), 
							delivery.getOrderNumberOther(), 
							delivery.getDeliveryTime(), 
							delivery.getLeftAt(), 
							delivery.getSignedBy(), 
							delivery.getSignatureImageName(), 
							delivery.getSignatureImageURL(), 
							delivery.getDeliveryImageName(), 
							delivery.getDeliveryImageURL(), 
							delivery.getPreDeliveryImageName(), 
							delivery.getPreDeliveryImageURL(), 
							delivery.getAddress().getAddressLine1(), 
							delivery.getAddress().getAddressLine2(), 
							delivery.getAddress().getCity(), 
							delivery.getAddress().getState(), 
							delivery.getAddress().getZip(), 
							delivery.getAddress().getCountry(),
							delivery.getShopAddress().getAddressLine1(), 
							delivery.getShopAddress().getAddressLine2(), 
							delivery.getShopAddress().getCity(), 
							delivery.getShopAddress().getState(), 
							delivery.getShopAddress().getZip(), 
							delivery.getShopAddress().getCountry(),
							delivery.getAddress().getLat(),
							delivery.getAddress().getLon(),
							delivery.getTargetDeliveryDate(), 
							delivery.getTargetDeliveryTime(), 
							delivery.getDriverName(), 
							delivery.getDeliveryService(), 
							delivery.getMerchant(), 
							delivery.getDeliveryNotes(), 
							delivery.getDeliveryCo(),
							delivery.getEstimatedDeliveryDate(),
							delivery.getEstimatedDeliveryTime(),
							delivery.getDeliveryCoTripID(),
							delivery.getDeliveryCoID(),
							delivery.getStatus(),
							delivery.getAttemptedReason(),
							delivery.getShopName(),
							true);
					
					if(response.getErrorMessage() != null)
						if(errorMessage.equals("Errors:"))
							errorMessage += " " + delivery.getOrderNumberBMS() +":"+ response.getErrorMessage() + ";";
						else
							errorMessage += ", " + delivery.getOrderNumberBMS() +":"+ response.getErrorMessage() + ";";
				}
				json.setErrorMessage(errorMessage);				
				json.setSuccessMessage("Bulk Upload Completed Successfully. See the errorMessage field for specific errors that may have occured during this bulk upload.");
			}
		}else{
			//Unauthorized
    		json.setErrorMessage(UNAUTHENTICATED);
			return json;
		}
		
		return json;
	}
	
    /* (non-Javadoc)
     * This is the entry point for inserting or updating delivery data. Response returned as JSON
     */
    @RequestMapping(value = "/v1/InsertOrUpdateDeliveryInfo", method = RequestMethod.POST) 
    @Scope("request")
    public @ResponseBody JSONInsertResponse insertOrUpdateDeliveryInfo(
			@RequestParam(value="apiKey", required=false) String apiKey,
			@RequestParam(value="fulfiller", required=false) String fulfiller,
			@RequestParam(value="deliveryDate", required=false) String deliveryDate,
			@RequestParam(value="orderNumberBLK", required=false) String orderNumberBLK,
			@RequestParam(value="orderNumberAtlas", required=false) String orderNumberAtlas,
			@RequestParam(value="orderNumberBMS", required=false) String orderNumberBMS,
			@RequestParam(value="externalId", required=false) String externalId,
			@RequestParam(value="orderNumberOther", required=false) String orderNumberOther,
			@RequestParam(value="deliveryTime", required=false) String deliveryTime,
			@RequestParam(value="leftAt", required=false) String leftAt,
			@RequestParam(value="signedBy", required=false) String signedBy,
			@RequestParam(value="signatureImageName", required=false) String signatureImageName,
			@RequestParam(value="signatureImageURL", required=false) String signatureImageURL,
			@RequestParam(value="deliveryImageName", required=false) String deliveryImageName,
			@RequestParam(value="deliveryImageURL", required=false) String deliveryImageURL,
			@RequestParam(value="preDeliveryImageName", required=false) String preDeliveryImageName,
			@RequestParam(value="preDeliveryImageURL", required=false) String preDeliveryImageURL,
			@RequestParam(value="addressLine1", required=false) String addressLine1,
			@RequestParam(value="addressLine2", required=false) String addressLine2,
			@RequestParam(value="city", required=false) String city,
			@RequestParam(value="state", required=false) String state,
			@RequestParam(value="zip", required=false) String zip,
			@RequestParam(value="country", required=false) String country,
			@RequestParam(value="shopAddressLine1", required=false) String shopAddressLine1,
			@RequestParam(value="shopAddressLine2", required=false) String shopAddressLine2,
			@RequestParam(value="shopCity", required=false) String shopCity,
			@RequestParam(value="shopState", required=false) String shopState,
			@RequestParam(value="shopZip", required=false) String shopZip,
			@RequestParam(value="shopCountry", required=false) String shopCountry,
			@RequestParam(value="lat", required=false) String lat,
			@RequestParam(value="lon", required=false) String lon,
			@RequestParam(value="targetDeliveryDate", required=false) String targetDeliveryDate,
			@RequestParam(value="targetDeliveryTime", required=false) String targetDeliveryTime,
			@RequestParam(value="driverName", required=false) String driverName,
			@RequestParam(value="deliveryService", required=false) String deliveryService,
			@RequestParam(value="merchant", required=false) String merchant,
			@RequestParam(value="deliveryNotes", required=false) String deliveryNotes,
			@RequestParam(value="deliveryCo", required=false) String deliveryCo,
			@RequestParam(value="estimatedDeliveryDate", required=false) String estimatedDeliveryDate,
			@RequestParam(value="estimatedDeliveryTime", required=false) String estimatedDeliveryTime,
			@RequestParam(value="deliveryCoTripID", required=false) String deliveryCoTripID,
			@RequestParam(value="deliveryCoID", required=false) String deliveryCoID,
			@RequestParam(value="status", required=false) String status,
			@RequestParam(value="attemptedReason", required=false) String attemptedReason,
			@RequestParam(value="shopName", required=false) String shopName,
			@RequestParam(value="jfkas6534d8SAD234G3smKZnalkLK6Zfgda39090TRTFjhj0", required=false) boolean authenticatedAlready
			) {
    	    	
    		//check if this is an authorized attempt
    		Map<String, String> userDetails = authorize(apiKey);
    		if(authenticatedAlready){
    			userDetails = new HashMap<String,String>();
    			userDetails.put("userID", "1");
    			userDetails.put("userName", "1");
    		}
    		JSONInsertResponse json = new JSONInsertResponse();
    		String orderNumber = null;
    		
			if(userDetails != null && userDetails.size() == 2){
				//authorized
				
				if(properties == null) loadProperties();
				
				if(usersList.contains(userDetails.get("userName")) && !merchant.equals(userDetails.get("userName"))){
					json.setErrorMessage("You may only save or update an order for merchant " + userDetails.get(1));
					return json;
				}
				
				//make sure fulfiller (delivering shop code) is provided, it is a required field
				if(fulfiller == null || fulfiller.trim().equals("")){
					json.setErrorMessage("You must provide a fulfiller (delivering shop code), please try again.");
					return json;
				}
				//make sure merchant (sending shop code) is provided, it is a required field
				if(merchant == null || merchant.trim().equals("")){
					json.setErrorMessage("You must provide a merchant (sending shop code), please try again.");
					return json;
				}
				
				//make sure target delivery date is provided, it is a required field
				if(targetDeliveryDate == null || targetDeliveryDate.trim().equals("")){
					json.setErrorMessage("You must provide a target delivery date, please try again.");
					return json;
				}
				
				//make sure recipient address is provided, it is a required field
				if(addressLine1 == null || addressLine1.trim().equals("")){
					json.setErrorMessage("You must provide an recipient address, please try again.");
					return json;
				}
				
				//make sure recipient city is provided, it is a required field
				if(city == null || city.trim().equals("")){
					json.setErrorMessage("You must provide a recipient city, please try again.");
					return json;
				}
				
				//make sure recipient state is provided, it is a required field
				if(state == null || state.trim().equals("")){
					json.setErrorMessage("You must provide a recipient state, please try again.");
					return json;
				}
				
				//make sure recipient zip code is provided, it is a required field
				if(zip == null || zip.trim().equals("")){
					json.setErrorMessage("You must provide a recipient zip code, please try again.");
					return json;
				}
				
				//make sure recipient country is provided, it is a required field
				if(country == null || country.trim().equals("")){
					json.setErrorMessage("You must provide a recipient country, please try again.");
					return json;
				}
				
				//make sure shop address is provided, it is a required field
				if(shopAddressLine1 == null || shopAddressLine1.trim().equals("")){
					json.setErrorMessage("You must provide a shop address, please try again.");
					return json;
				}
				
				//make sure shop city is provided, it is a required field
				if(shopCity == null || shopCity.trim().equals("")){
					json.setErrorMessage("You must provide a shop city, please try again.");
					return json;
				}
				
				//make sure shop state is provided, it is a required field
				if(shopState == null || shopState.trim().equals("")){
					json.setErrorMessage("You must provide a shop state, please try again.");
					return json;
				}
				
				//make sure shop zip code is provided, it is a required field
				if(shopZip == null || shopZip.trim().equals("")){
					json.setErrorMessage("You must provide a shop zip code, please try again.");
					return json;
				}
				
				//make sure shop country is provided, it is a required field
				if(shopCountry == null || shopCountry.trim().equals("")){
					json.setErrorMessage("You must provide a shop country, please try again.");
					return json;
				}
				
				//make sure fulfilling shop name is provided, it is a required field
				if(shopName == null || shopName.trim().equals("")){
					json.setErrorMessage("You must provide a shop name, please try again.");
					return json;
				}
				
				if(orderNumberAtlas != null){
					orderNumberAtlas = orderNumberAtlas.trim().replaceAll("\t","");
				}
				
				if(orderNumberBLK != null){
					orderNumberBLK = orderNumberBLK.trim().replaceAll("\t","");
				}
				
				if(orderNumberBMS != null){
					orderNumberBMS = orderNumberBMS.trim().replaceAll("\t","");
				}
				
				if(orderNumberOther != null){
					orderNumberOther = orderNumberOther.trim().replaceAll("\t","");
				}
					
				
				Delivery deliveryData = null;
	    		boolean atLeastOneIdentifier = false;
	    		boolean createNewRecord = false;
	    		
	    		//create a list of all provided order numbers
	    		List<String> orderNumbers = new ArrayList<String>();
	    		orderNumbers.add(orderNumberBLK);
	    		orderNumbers.add(orderNumberAtlas);
	    		orderNumbers.add(orderNumberBMS);
	    		orderNumbers.add(orderNumberOther);
	    		
	    		//loop through the list, if an order number is not null, set the boolean to true (at least 1 order number provided)
	    		//then look up the order number to see if it already exists in the database
	    		for(int ii=0; ii<orderNumbers.size(); ++ii){
	    		
		    		if(orderNumbers.get(ii) != null && !orderNumbers.get(ii).equals("")){
		    			atLeastOneIdentifier = true;
		    			List<Delivery> deliveries = dao.getDeliveriesByOrderNumber(orderNumbers.get(ii));
		    			orderNumber = orderNumbers.get(ii);
		    			if(deliveries != null && deliveries.size() > 0){
		    				deliveryData = deliveries.get(0);
		    				break;
		    			}
		    		}
	    		
	    		}
	    		//Make sure that deliveryCo matches a list of pre-determined values
	    		if(deliveryCo != null && !deliveryCo.trim().equals("") && !properties.isEmpty()){
	    			boolean valueMatched = false;
	    			String[] validValues = ((String) properties.get("deliveryCos")).split(",");
	    			for(int ii=0; ii<validValues.length; ++ii){
	    				if(deliveryCo.equalsIgnoreCase(validValues[ii]))
	    					valueMatched = true;
	    			}
	    			if(!valueMatched){
	    				json.setErrorMessage("An invalid value was submitted for deliveryCo. Please see the user documentation for valid values.");
	    				return json;
	    			}
	    		}
	    		
	    		if(externalId != null && !externalId.trim().equals(""))
	    			atLeastOneIdentifier = true;
	    		
	    		//At least one order number needs to be provided
	    		if(!atLeastOneIdentifier){
	    			json.setErrorMessage("You must provide an order number or externalId and deliveryCo, please try again.");
					return json;
	    		}
	    		
	    		//Make sure deliveryDate is standardized
	    		try{
	    			String[] deliveryDateArray = deliveryDate.split("/");
	    			String month = deliveryDateArray[0];
	    			String day = deliveryDateArray[1];
	    			String year =deliveryDateArray[2];
	    			if(month.length() == 2 && Integer.valueOf(month) <= 12 && Integer.valueOf(month) >= 1 && day.length() == 2 && Integer.valueOf(day) <= 31 && Integer.valueOf(day) >= 1 && year.length() == 4 && Integer.valueOf(year) >= 2015){
	    				//confirmed
	    			}else{
	    				json.setErrorMessage("deliveryDate was not formatted properly. Please try again using the following format: MM/dd/yyyy");
						return json;
	    			}
	    		}catch(Exception ee){
	    			json.setErrorMessage("deliveryDate was not formatted properly. Please try again using the following format: MM/dd/yyyy");
					return json;
	    		}
	    		//Make sure targetDeliveryDate is standardized
	    		try{
	    			String[] targetDeliveryDateArray = targetDeliveryDate.split("/");
	    			String month = targetDeliveryDateArray[0];
	    			String day = targetDeliveryDateArray[1];
	    			String year =targetDeliveryDateArray[2];
	    			if(month.length() == 2 && Integer.valueOf(month) <= 12 && Integer.valueOf(month) >= 1 && day.length() == 2 && Integer.valueOf(day) <= 31 && Integer.valueOf(day) >= 1 && year.length() == 4 && Integer.valueOf(year) >= 2015){
	    				//confirmed
	    			}else{
	    				json.setErrorMessage("targetDeliveryDate was not formatted properly. Please try again using the following format: MM/dd/yyyy");
						return json;
	    			}
	    		}catch(Exception ee){
	    			json.setErrorMessage("targetDeliveryDate was not formatted properly. Please try again using the following format: MM/dd/yyyy");
					return json;
	    		}
	    		
	    		//this is just for file name consistency
	    		if(signatureImageName != null)
	    			signatureImageName = signatureImageName.toLowerCase();
	    		
	    		String previousStatus = "";
	    		
	    		if(deliveryData == null){
	    			deliveryData = new Delivery();
	    			createNewRecord = true;
	    		}else if(deliveryData.getStatus() != null){
	    			previousStatus = deliveryData.getStatus();
	    		}
	    		
	    		//recipient address information
	    		Address addressInfo = null;
	    		
	    		List<Address> existingAddresses = dao.getAddress(addressLine1, addressLine2, city, state, zip, country);
	    		if(existingAddresses != null && existingAddresses.size() > 0)
	    			addressInfo = existingAddresses.get(0);
	    		else{
	    			addressInfo = new Address();
		    		addressInfo.setAddressLine1(addressLine1);
		    		addressInfo.setAddressLine2(addressLine2);
		    		addressInfo.setCity(city);
		    		addressInfo.setState(state);
		    		addressInfo.setZip(zip);
		    		addressInfo.setCountry(country);
		    		addressInfo.setLat(lat);
		    		addressInfo.setLon(lon);
		    		dao.addAddress(addressInfo);
	    		}
	    		
	    		//shop address information
	    		Address shopAddressInfo = null;
	    		
	    		List<Address> existingShopAddresses = dao.getAddress(shopAddressLine1, shopAddressLine2, shopCity, shopState, shopZip, shopCountry);
	    		if(existingShopAddresses != null && existingShopAddresses.size() > 0)
	    			shopAddressInfo = existingShopAddresses.get(0);
	    		else{
	    			shopAddressInfo = new Address();
	    			shopAddressInfo.setAddressLine1(shopAddressLine1);
	    			shopAddressInfo.setAddressLine2(shopAddressLine2);
	    			shopAddressInfo.setCity(shopCity);
	    			shopAddressInfo.setState(shopState);
	    			shopAddressInfo.setZip(shopZip);
	    			shopAddressInfo.setCountry(shopCountry);
		    		dao.addAddress(shopAddressInfo);
	    		}
	    		
	    		//set all of the data provided to the Delivery object
	    		deliveryData.setAddress(addressInfo);
	    		deliveryData.setShopAddress(shopAddressInfo);
	    		deliveryData.setMerchant(merchant.toUpperCase());
	    		deliveryData.setFulfiller(fulfiller.toUpperCase());
	    		deliveryData.setTargetDeliveryDate(targetDeliveryDate);
	    		deliveryData.setTargetDeliveryTime(targetDeliveryTime);
	    		deliveryData.setDeliveryDate(deliveryDate);
	    		deliveryData.setDeliveryTime(deliveryTime);
	    		deliveryData.setDriverName(driverName);
	    		deliveryData.setDeliveryService(deliveryService);
	    		deliveryData.setDeliveryCo(deliveryCo);
	    		deliveryData.setDeliveryNotes(deliveryNotes);
	    		deliveryData.setLeftAt(leftAt);
	    		deliveryData.setOrderNumberAtlas(orderNumberAtlas);
	    		deliveryData.setOrderNumberBLK(orderNumberBLK);
	    		deliveryData.setOrderNumberBMS(orderNumberBMS);
	    		deliveryData.setExternalId(externalId);
	    		deliveryData.setOrderNumberOther(orderNumberOther);
	    		deliveryData.setSignatureImageName(signatureImageName);
	    		deliveryData.setSignatureImageURL(signatureImageURL);
	    		deliveryData.setDeliveryImageName(deliveryImageName);
	    		deliveryData.setDeliveryImageURL(deliveryImageURL);
	    		deliveryData.setPreDeliveryImageName(preDeliveryImageName);
	    		deliveryData.setPreDeliveryImageURL(preDeliveryImageURL);
	    		deliveryData.setEstimatedDeliveryDate(estimatedDeliveryDate);
	    		deliveryData.setEstimatedDeliveryTime(estimatedDeliveryTime);
	    		deliveryData.setDeliveryCoTripID(deliveryCoTripID);
	    		deliveryData.setDeliveryCoID(deliveryCoID);
	    		deliveryData.setStatus(status);
	    		deliveryData.setAttemptedReason(attemptedReason);
	    		deliveryData.setShopName(shopName);
	    		if(signatureImageName != null && !signatureImageName.trim().equals("")){
	    			deliveryData.setSignatureImageURL("https://delivery.bloomnet.net/images/"+signatureImageName);
	    		}
	    		if(deliveryImageName != null && !deliveryImageName.trim().equals("")){
	    			deliveryData.setDeliveryImageURL("https://delivery.bloomnet.net/images/"+deliveryImageName);
	    		}
	    		if(preDeliveryImageName != null && !preDeliveryImageName.trim().equals("")){
	    			deliveryData.setDeliveryImageURL("https://delivery.bloomnet.net/images/"+preDeliveryImageName);
	    		}
	    		deliveryData.setSignedBy(signedBy);
	    		deliveryData.setUserID(userDetails.get("userID"));
	    		
	    		//the timestamp for insertion Created Date or Modified Date
	    		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a");
	    		String date = format.format(new Date());
	    		
	    		if(createNewRecord){//create new record
	    			deliveryData.setCreatedDate(date);
	    			if(lat == null || lon == null || lat.equals("") || lon.equals("")){
	    				try{
	    					String googleJsonResponse = new GeoCode().geoCode(addressLine1+","+city+","+state+","+zip);
	    					lat = googleJsonResponse.split("\"lat\" : ")[1].split(",")[0];
	    					lon = googleJsonResponse.split("\"lng\" : ")[1].split("\n")[0];
	    					addressInfo.setLat(lat);
	    					addressInfo.setLon(lon);
	    					deliveryData.setAddress(addressInfo);
	    				}catch(Exception ee){}
	    			}
	    			dao.addDelivery(deliveryData);
	    			initiateWebhooks(merchant, deliveryData, apiKey);
					DeliveryHistory deliveryDataHistory = CopyDeliveryToHistory.getHistoryFromDelivery(deliveryData);
	    			dao.addDeliveryHistory(deliveryDataHistory);
	    		}else{//update an existing record
	    			deliveryData.setModifiedDate(date);
	    			dao.updateDelivery(deliveryData);
	    			if(status != null && !status.equals(previousStatus)){
		    			initiateWebhooks(merchant, deliveryData, apiKey);
	    				deliveryData.setModifiedDate(null);
		    			deliveryData.setCreatedDate(date);
	    				DeliveryHistory deliveryDataHistory = CopyDeliveryToHistory.getHistoryFromDelivery(deliveryData);
	    				dao.addDeliveryHistory(deliveryDataHistory);
	    			}
	    		}
				
			}else{
				//unauthorized
				json.setErrorMessage(UNAUTHENTICATED);
				return json;
			}
			json.setSuccessMessage("Success! The delivery information you submitted has been saved.");
			json.setOrderNumber(orderNumber);
	        return json;  
    }  
    
    /* (non-Javadoc)
     * This is the entry point for retrieving delivery data (without images) returned as JSON
     */
    @RequestMapping(value = "/v1/RetrieveDeliveryInfoByOrderNumber", method = RequestMethod.GET)  
	public @ResponseBody JSONRetrievalResponse retrieveDeliveryInfoByOrderNumber(
			@RequestParam(value="apiKey", required=false) String apiKey,
			@RequestParam(value="orderNumber", required=false) String orderNumber
			) {
    	
    	JSONRetrievalResponse json = new JSONRetrievalResponse();
    	//Check to see if this is an authenticated attempt
    	Map<String, String> userDetails = authorize(apiKey);
    	
    	if(userDetails != null && userDetails.size() == 2){
    		//authenticated
    		
    		//Check if order number was provided, this is required
    		if(orderNumber == null || orderNumber.trim().equals("")){
    			json.setErrorMessage("An order number was not specified. Please try again after correcting your request.");
    			return json;
    		}
    		
    		//get the delivery data from the database, as a list
    		List<Delivery> deliveries = dao.getDeliveriesByOrderNumber(orderNumber);
    		
    		if(deliveries != null && deliveries.size() > 0){
    			
    			//get the first result
    			Delivery delivery = deliveries.get(0);
    			if(usersList.contains(userDetails.get("userName")) && !delivery.getMerchant().equals(userDetails.get("userName")))
    				json.setErrorMessage("No delivery was found for the provided order number.");
    			else
    				json = createJSON(delivery);	    			
    			
    		}else{
    			json.setErrorMessage("No delivery was found for the provided order number.");
    		}
    		
    	}else{
    		//Unauthorized
    		json.setErrorMessage(UNAUTHENTICATED);
			return json;
    	}
    	
    	return json;  
    }
    
    /* (non-Javadoc)
     * This is the entry point for retrieving delivery data history (without images) returned as JSON
     */
    @RequestMapping(value = "/v1/RetrieveDeliveryInfoHistoryByOrderNumber", method = RequestMethod.GET)  
	public @ResponseBody List<JSONRetrievalResponse> retrieveDeliveryInfoHistoryByOrderNumber(
			@RequestParam(value="apiKey", required=false) String apiKey,
			@RequestParam(value="orderNumber", required=false) String orderNumber
			) {
    	
    	List<JSONRetrievalResponse> jsonList = new ArrayList<JSONRetrievalResponse>();
    	JSONRetrievalResponse json = new JSONRetrievalResponse();
    	//Check to see if this is an authenticated attempt
    	Map<String, String> userDetails = authorize(apiKey);
    	
    	if(userDetails != null && userDetails.size() == 2){
    		//authenticated
    		
    		//Check if order number was provided, this is required
    		if(orderNumber == null || orderNumber.trim().equals("")){
    			json.setErrorMessage("An order number was not specified. Please try again after correcting your request.");
    			jsonList.add(json);
    			return jsonList;
    		}
    		
    		//get the delivery data from the database, as a list
    		List<DeliveryHistory> deliveries = dao.getDeliveriesHistoryByOrderNumber(orderNumber);
    		
    		if(deliveries != null && deliveries.size() > 0){
    			for(DeliveryHistory delivery : deliveries){
    				if(usersList.contains(userDetails.get("userName")) && !delivery.getMerchant().equals(userDetails.get("userName"))){}
        				
        			else{
        				json = createJSON(delivery);
        				jsonList.add(json);
        			}
    			}
    			if(jsonList.size() == 0){
    				json.setErrorMessage("No delivery history was found for the provided order number.");
    				jsonList.add(json);
    			}
    		}else{
    			json.setErrorMessage("No delivery history was found for the provided order number.");
    			jsonList.add(json);
    			return jsonList;
    		}
    		
    	}else{
    		//Unauthorized
    		json.setErrorMessage(UNAUTHENTICATED);
    		jsonList.add(json);
			return jsonList;
    	}
    	
    	return jsonList;  
    }
    
   /* @RequestMapping(value = "/v1/RetrieveDeliveryInfoByExternalId", method = RequestMethod.GET)  
   	public @ResponseBody List<JSONRetrievalResponse> retrieveDeliveryInfoByExternalId(
   			@RequestParam(value="apiKey", required=false) String apiKey,
   			@RequestParam(value="externalId", required=false) String externalId,
   			@RequestParam(value="deliveryCo", required=false) String deliveryCo
   			) {
    	List<JSONRetrievalResponse> results = new ArrayList<JSONRetrievalResponse>();
       	JSONRetrievalResponse json = new JSONRetrievalResponse();
       	//Check to see if this is an authenticated attempt
       	String userDetails = authorize(apiKey);
       	
       	if(userDetails != null && userDetails.size() == 2){
       		//authenticated
       		
       		//Check if order number was provided, this is required
       		if(externalId == null || externalId.trim().equals("")){
       			json.setErrorMessage("An externalId was not specified. Please try again after correcting your request.");
       			results.add(json);
       			return results;
       		}
       		
       		if(deliveryCo == null || deliveryCo.trim().equals("")){
       			json.setErrorMessage("A deliveryCo was not specified. Please try again after correcting your request.");
       			results.add(json);
       			return results;
       		}
       		
       		loadProperties();
       		if(!properties.isEmpty()){
	       		boolean valueMatched = false;
				String[] validValues = ((String) properties.get("deliveryCos")).split(",");
				for(int ii=0; ii<validValues.length; ++ii){
					if(deliveryCo.equalsIgnoreCase(validValues[ii]))
						valueMatched = true;
				}
				if(!valueMatched){
					json.setErrorMessage("An invalid value was submitted for deliveryCo. Please see the user documentation for valid values.");
					results.add(json);
	       			return results;
				}
       		}
       		
       		//get the delivery data from the database, as a list
       		List<Delivery> deliveries = dao.getDeliveriesByExternalId(externalId, deliveryCo);
       		
       		if(deliveries != null && deliveries.size() > 0){
       			
       			for(int ii = 0; ii < deliveries.size(); ++ ii){
    				Delivery delivery = deliveries.get(ii);
	    			json = createJSON(delivery);	    			
	    			results.add(json);
    			}    			
       			
       		}else{
       			json.setErrorMessage("No delivery was found for the provided externalId and deliveryCo.");
       		}
       		
       	}else{
       		//Unauthorized
       		json.setErrorMessage(UNAUTHENTICATED);
       		results.add(json);
   			return results;
       	}
       	
		return results; 
       } */
    
    /*@RequestMapping(value = "/v1/RetrieveDeliveryInfoByDateRange", method = RequestMethod.GET)  
	public @ResponseBody List<JSONRetrievalResponse> retrieveDeliveryInfoByDateRange(
			@RequestParam(value="apiKey", required=false) String apiKey,
			@RequestParam(value="startDate", required=false) String startD,
			@RequestParam(value="endDate", required=false) String endD
			) {
    	List<JSONRetrievalResponse> results = new ArrayList<JSONRetrievalResponse>();
    	JSONRetrievalResponse json = new JSONRetrievalResponse();
    	//Check to see if this is an authenticated attempt
    	String userDetails = authorize(apiKey);
    	
    	if(userDetails != null && userDetails.size() == 2){
    		//authenticated
    		
    		try{
    			String[] startDateArray = startD.split("/");
    			String month = startDateArray[0];
    			String day = startDateArray[1];
    			String year =startDateArray[2];
    			if(month.length() == 2 && Integer.valueOf(month) <= 12 && Integer.valueOf(month) >= 1 && day.length() == 2 && Integer.valueOf(day) <= 31 && Integer.valueOf(day) >= 1 && year.length() == 4 && Integer.valueOf(year) >= 2015){
    				//confirmed
    			}else{
    				json.setErrorMessage("startDate was not formatted properly. Please try again using the following format: MM/dd/yyyy");
					results.add(json);
    				return results;
    			}
    		}catch(Exception ee){
    			json.setErrorMessage("startDate was not formatted properly. Please try again using the following format: MM/dd/yyyy");
    			results.add(json);
				return results;
    		}
    		
    		try{
    			String[] endDateArray = endD.split("/");
    			String month = endDateArray[0];
    			String day = endDateArray[1];
    			String year =endDateArray[2];
    			if(month.length() == 2 && Integer.valueOf(month) <= 12 && Integer.valueOf(month) >= 1 && day.length() == 2 && Integer.valueOf(day) <= 31 && Integer.valueOf(day) >= 1 && year.length() == 4 && Integer.valueOf(year) >= 2015){
    				//confirmed
    			}else{
    				json.setErrorMessage("endDate was not formatted properly. Please try again using the following format: MM/dd/yyyy");
    				results.add(json);
    				return results;
    			}
    		}catch(Exception ee){
    			json.setErrorMessage("endDate was not formatted properly. Please try again using the following format: MM/dd/yyyy");
    			results.add(json);
				return results;
    		}
    		
    		//get the delivery data from the database, as a list
    		List<Delivery> deliveries = dao.getDeliveriesByDateRange(startD, endD);
    		
    		if(deliveries != null && deliveries.size() > 0){
    			
    			for(int ii = 0; ii < deliveries.size(); ++ ii){
    				Delivery delivery = deliveries.get(ii);
	    			json = createJSON(delivery);	    			
	    			results.add(json);
    			}
    			
    		}else{
    			json.setErrorMessage("No deliveries were found for the provided date range.");
    			results.add(json);
    			return results;
    		}
    		
    	}else{
    		//Unauthorized
    		json.setErrorMessage(UNAUTHENTICATED);
    		results.add(json);
			return results;
    	}
    	
    	return results;  
    } */
    
    /*@RequestMapping(value = "/v1/RetrieveDeliveryInfoByMerchant", method = RequestMethod.GET)  
	public @ResponseBody List<JSONRetrievalResponse> retrieveDeliveryInfoByMerchant(
			@RequestParam(value="apiKey", required=false) String apiKey,
			@RequestParam(value="merchant", required=false) String merchant
			) {
    	List<JSONRetrievalResponse> results = new ArrayList<JSONRetrievalResponse>();
    	JSONRetrievalResponse json = new JSONRetrievalResponse();
    	//Check to see if this is an authenticated attempt
    	String userDetails = authorize(apiKey);
    	
    	if(userDetails != null && userDetails.size() == 2){
    		//authenticated
    		
    		//Check if merchant was provided, this is required
    		if(merchant == null || merchant.trim().equals("")){
    			json.setErrorMessage("A merchant was not specified. Please try again after correcting your request.");
    			results.add(json);
    			return results;
    		}
    		
    		//get the delivery data from the database, as a list
    		List<Delivery> deliveries = dao.getDeliveriesByMerchant(merchant);
    		
    		if(deliveries != null && deliveries.size() > 0){
    			
    			for(int ii = 0; ii < deliveries.size(); ++ ii){
    				Delivery delivery = deliveries.get(ii);
	    			json = createJSON(delivery);	    			
	    			results.add(json);
    			}
    			
    		}else{
    			json.setErrorMessage("No deliveries were found for the provided merchant.");
    			results.add(json);
    			return results;
    		}
    		
    	}else{
    		//Unauthorized
    		json.setErrorMessage(UNAUTHENTICATED);
    		results.add(json);
			return results;
    	}
    	
    	return results;  
    } */
    
    /*@RequestMapping(value = "/v1/RetrieveDeliveryInfoByFulfiller", method = RequestMethod.GET)  
	public @ResponseBody List<JSONRetrievalResponse> retrieveDeliveryInfoByFulfiller(
			@RequestParam(value="apiKey", required=false) String apiKey,
			@RequestParam(value="fulfiller", required=false) String fulfiller
			) {
    	List<JSONRetrievalResponse> results = new ArrayList<JSONRetrievalResponse>();
    	JSONRetrievalResponse json = new JSONRetrievalResponse();
    	//Check to see if this is an authenticated attempt
    	String userDetails = authorize(apiKey);
    	
    	if(userDetails != null && userDetails.size() == 2){
    		//authenticated
    		
    		//Check if merchant was provided, this is required
    		if(fulfiller == null || fulfiller.trim().equals("")){
    			json.setErrorMessage("A fulfiller was not specified. Please try again after correcting your request.");
    			results.add(json);
    			return results;
    		}
    		
    		//get the delivery data from the database, as a list
    		List<Delivery> deliveries = dao.getDeliveriesByFulfiller(fulfiller);
    		if(deliveries != null && deliveries.size() > 0){
    			
    			for(int ii = 0; ii < deliveries.size(); ++ ii){
	    			Delivery delivery = deliveries.get(ii);
	    			json = createJSON(delivery);	    			
	    			results.add(json);
    			}
    			
    		}else{
    			json.setErrorMessage("No deliveries were found for the provided merchant.");
    			results.add(json);
    			return results;
    		}
    		
    	}else{
    		//Unauthorized
    		json.setErrorMessage(UNAUTHENTICATED);
    		results.add(json);
			return results;
    	}
    	
    	return results;  
    } */
    
    /* (non-Javadoc)
     * This is the entry point for retrieving delivery images returned as a page
     */
    @RequestMapping(value = "/v1/RetrieveDeliveryImages", method = RequestMethod.GET)  
    public String retrieveDeliveryImages(
			@ModelAttribute Delivery deliveryData, ModelMap model,
			@RequestParam(value="orderNumber", required=false) String orderNumber,
			@RequestParam(value="externalId", required=false) String externalId,
   			@RequestParam(value="deliveryCo", required=false) String deliveryCo
			) {  
    		
    	List<Delivery> deliveries = null;
    	
    	if(orderNumber != null && !orderNumber.trim().equals(""))
				deliveries = dao.getDeliveriesByOrderNumber(orderNumber);
    	else if(externalId != null && !externalId.trim().equals("") && deliveryCo != null && !deliveryCo.equals(""))
    		deliveries = dao.getDeliveriesByExternalId(externalId, deliveryCo);
		
		if(deliveries != null && deliveries.size() > 0){
			
			Delivery delivery = deliveries.get(0);
			model.addAttribute("deliveryData",delivery);//add the Delivery object to the model for the JSP page
			
		}else{
			model.addAttribute("errorMessage","No delivery images were found for this order");
		}
    		
        return "showImages";//directs to the showImages.jsp page
    }
    
    @RequestMapping(value = "/v1/RetrieveDeliveryImage", method = RequestMethod.GET, produces = MediaType.IMAGE_JPEG_VALUE)  
    public @ResponseBody byte[] retrieveDeliveryImage (
			@RequestParam(value="orderNumber", required=false) String orderNumber,
			@RequestParam(value="apiKey", required=false) String apiKey
			) throws IOException {  
    	
    	Map<String, String> userDetails = authorize(apiKey);
      	
      	if(userDetails != null && userDetails.size() == 2){
    	
	    	List<Delivery> deliveries = null;
	    	
	    	if(orderNumber != null && !orderNumber.trim().equals(""))
					deliveries = dao.getDeliveriesByOrderNumber(orderNumber);
			
			if(deliveries != null && deliveries.size() > 0){
				
				try{
					
				
					Delivery delivery = deliveries.get(0);
					String deliveryImageURL = delivery.getDeliveryImageURL();
					if(deliveryImageURL != null && !deliveryImageURL.equals("") ){
						URL url = new URL(deliveryImageURL);
					    InputStream in = url.openStream();
						return IOUtils.toByteArray(in);
					}	
				}catch(Exception ee){
					return null;
				}
			}
      	}	
		return null;
    }
    
    @RequestMapping(value = "/v1/RetrieveSignatureImage", method = RequestMethod.GET, produces = MediaType.IMAGE_JPEG_VALUE)  
    public @ResponseBody byte[] retrieveSignatureImage(
			@RequestParam(value="orderNumber", required=false) String orderNumber,
			@RequestParam(value="apiKey", required=false) String apiKey
			) throws IOException {  
    		
    	Map<String, String> userDetails = authorize(apiKey);
      	
      	if(userDetails != null && userDetails.size() == 2){
    	
	    	List<Delivery> deliveries = null;
	    	
	    	if(orderNumber != null && !orderNumber.trim().equals(""))
					deliveries = dao.getDeliveriesByOrderNumber(orderNumber);
			
			if(deliveries != null && deliveries.size() > 0){
				
				try{
				
					Delivery delivery = deliveries.get(0);
					String signatureImageURL = delivery.getSignatureImageURL();
					if(signatureImageURL != null && !signatureImageURL.equals("") ){
						URL url = new URL(signatureImageURL);
					    InputStream in = url.openStream();
						return IOUtils.toByteArray(in);
					}	
				}catch(Exception ee){
					return null;
				}
			}
      	}	
		return null;
    }
    
    /* (non-Javadoc)
     * This is the entry point for retrieving delivery addresses returned as a page with a map
     */
    @RequestMapping(value = "/v1/RetrieveDeliveries", method = RequestMethod.GET)  
    public String retrieveDeliveries(
			@ModelAttribute Delivery deliveryData, ModelMap model,
			@RequestParam(value="targetDeliveryDate", required=false) String date,
			@RequestParam(value="fulfiller", required=false) String fulfiller,
			@RequestParam(value="merchant", required=false) String merchant,
			@RequestParam(value="orderNumbers", required=false) String orderNumbers,
			@RequestParam(value="externalIDs", required=false) String externalIDs,
			@RequestParam(value="deliveryCo", required=false) String deliveryCo,
			@RequestParam(value="apiKey", required=false) String apiKey
			) {  
    	
    	Map<String, String> userDetails = authorize(apiKey);
      	
      	if(userDetails != null && userDetails.size() == 2){
    	
	    	List<Delivery> deliveries = null;
	    	
	    	if(userDetails.get("userName").equals("00800000")) merchant = "00800000";
	    	
	    	if(date != null && !date.trim().equals("")){
	    		try{
	    			String [] dateParts = date.split("/");
	    			String date1 = "";
	    			String date2 = "";
	    			if(dateParts[0].length() == 1)
	    				date1 = "0" + dateParts[0];
	    			else
	    				date1 = dateParts[0];
	    			if(dateParts[1].length() == 1)
	    				date2 = "0" + dateParts[1];
	    			else
	    				date2 = dateParts[1];
	    			date = date1 + "/" + date2 + "/" + dateParts[2];
	    			if(usersList.contains(userDetails.get("userName")))
	    				merchant = userDetails.get("userName");
	    		}catch(Exception ee){}
		    	if(fulfiller != null && !fulfiller.trim().equals(""))
		    		deliveries = dao.getDeliveriesByFulfillerAndTargetDeliveryDate(fulfiller,date);
		    	else if(merchant != null && !merchant.trim().equals(""))
		    		deliveries = dao.getDeliveriesByMerchantAndTargetDeliveryDate(merchant,date);
		    	else if(date != null && !date.trim().equals(""))
		    		deliveries = dao.getDeliveriesByTargetDeliveryDate(date);
	    	}else if(orderNumbers != null && !orderNumbers.equals(""))
	    		deliveries = dao.getDeliveriesByOrderNumbers(orderNumbers);
	    	else if(externalIDs != null && !externalIDs.equals("") && deliveryCo != null && !deliveryCo.equals(""))
	    		deliveries = dao.getDeliveriesByExternalIDs(externalIDs, deliveryCo);
	    	
			if(deliveries != null && deliveries.size() > 0){
				
				model.addAttribute("deliveryData",deliveries);//add the Delivery objects to the model for the JSP page
				
			}else{
				model.addAttribute("errorMessage","No delivery addresses were found for your criteria.");
			}
	    		
	        return "showMap";//directs to the shopMap.jsp page
      	}else{
      		return null;
      	}
    }
    
   /* @RequestMapping(value = "/v1/RetrieveAddressesByShopCodeAndDeliveryDate", method = RequestMethod.GET)  
  	public @ResponseBody List<JSONRetrievalResponse> retrieveAddressesByShopCodeAndDeliveryDate(
  			@RequestParam(value="apiKey", required=false) String apiKey,
  			@RequestParam(value="shopCode", required=false) String shopCode,
  			@RequestParam(value="deliveryDate", required=false) String date
  			) {
      	List<JSONRetrievalResponse> results = new ArrayList<JSONRetrievalResponse>();
      	JSONRetrievalResponse json = new JSONRetrievalResponse();
      	//Check to see if this is an authenticated attempt
      	String userDetails = authorize(apiKey);
      	
      	if(userDetails != null && userDetails.size() == 2){
      		//authenticated
      		
      		//Check if merchant was provided, this is required
      		if(shopCode == null || shopCode.trim().equals("")){
      			json.setErrorMessage("A shop code was not specified. Please try again after correcting your request.");
      			results.add(json);
      			return results;
      		}
      		
      		if(date == null || date.trim().equals("")){
      			json.setErrorMessage("A delivery date was not specified. Please try again after correcting your request.");
      			results.add(json);
      			return results;
      		}
      		
      		//get the delivery data from the database, as a list
      		List<Delivery> deliveries = dao.getDeliveriesByFulfillerAndTargetDeliveryDate(shopCode, date);
      		if(deliveries != null && deliveries.size() > 0){
      			
      			for(int ii = 0; ii < deliveries.size(); ++ ii){
  	    			Delivery delivery = deliveries.get(ii);
  	    			json = createJSON(delivery);	    			
  	    			results.add(json);
      			}
      			
      		}else{
      			json.setErrorMessage("No deliveries were found for the provided shop code and delivery date.");
      			results.add(json);
      			return results;
      		}
      		
      	}else{
      		//Unauthorized
      		json.setErrorMessage(UNAUTHENTICATED);
      		results.add(json);
  			return results;
      	}
      	
      	return results;  
      } */
    
    /*@RequestMapping(value = "/v1/RetrieveAddressesByDeliveryDate", method = RequestMethod.GET)  
  	public @ResponseBody List<JSONRetrievalResponse> retrieveAddressesByDeliveryDate(
  			@RequestParam(value="apiKey", required=false) String apiKey,
  			@RequestParam(value="deliveryDate", required=false) String date
  			) {
      	List<JSONRetrievalResponse> results = new ArrayList<JSONRetrievalResponse>();
      	JSONRetrievalResponse json = new JSONRetrievalResponse();
      	//Check to see if this is an authenticated attempt
      	String userDetails = authorize(apiKey);
      	
      	if(userDetails != null && userDetails.size() == 2){
      		//authenticated
      		
      		if(date == null || date.trim().equals("")){
      			json.setErrorMessage("A delivery date was not specified. Please try again after correcting your request.");
      			results.add(json);
      			return results;
      		}
      		
      		//get the delivery data from the database, as a list
      		List<Delivery> deliveries = dao.getDeliveriesByTargetDeliveryDate(date);
      		if(deliveries != null && deliveries.size() > 0){
      			
      			for(int ii = 0; ii < deliveries.size(); ++ ii){
  	    			Delivery delivery = deliveries.get(ii);
  	    			json = createJSON(delivery);	    			
  	    			results.add(json);
      			}
      			
      		}else{
      			json.setErrorMessage("No deliveries were found for the provided shop code and delivery date.");
      			results.add(json);
      			return results;
      		}
      		
      	}else{
      		//Unauthorized
      		json.setErrorMessage(UNAUTHENTICATED);
      		results.add(json);
  			return results;
      	}
      	
      	return results;  
      }*/
    
    @RequestMapping(value = "/v1/RetreiveGeoCoding", method = RequestMethod.GET)  
  	public @ResponseBody String retrieveGeoCoding(
  			@RequestParam(value="apiKey", required=false) String apiKey,
  			@RequestParam(value="address", required=false) String address
  			) {
    	
      	List<JSONRetrievalResponse> results = new ArrayList<JSONRetrievalResponse>();
      	JSONRetrievalResponse json = new JSONRetrievalResponse();
      	//Check to see if this is an authenticated attempt
      	Map<String, String> userDetails = authorize(apiKey);
      	
      	if(userDetails != null && userDetails.size() == 2){
      		//authenticated
      		
      		if(address != null && !address.trim().equals("")){
      			
      			String geocoding = new GeoCode().geoCode(address);
      			
      			return geocoding;
      		}
      		
      		
      	}else{
      		//Unauthorized
      		json.setErrorMessage(UNAUTHENTICATED);
      		results.add(json);
  			return null;
      	}
      	
      	return null;  
      }
    
    /* (non-Javadoc)
     * This is the entry point for submitting the form data from the uploadImages.jsp page (POST request)
     */
    @RequestMapping(value = "/v1/SaveImage", method = RequestMethod.POST)  
   	public @ResponseBody JSONInsertResponse saveImage(
   			@RequestParam("file") MultipartFile file,
   			@RequestParam(value="apiKey", required=false) String apiKey
   			) {  
       	
    	//Check if this attempt is authorized
    	Map<String, String> userDetails = authorize(apiKey);
       	JSONInsertResponse json = new JSONInsertResponse();
       	if(userDetails == null || userDetails.size() < 2){
       		//unauthorized
       		json.setErrorMessage(UNAUTHENTICATED);
       	}else{
       		//authorized
       		
        	String fileName = null;
        	if (!file.isEmpty()) {
                try {
               
                    fileName = file.getOriginalFilename().toLowerCase();
                    
                    //Throw an error if the file is greater than 1MB
                    if(file.getSize() > 1000000)
                		throw new Exception("The file size exceeds 1MB. ");
                    
                    //Throw an error if the file is not an image type
                    if(!(fileName.endsWith(".jpg") || fileName.endsWith(".jpeg") || fileName.endsWith(".png") || fileName.endsWith(".tif") || fileName.endsWith(".bmp") || fileName.endsWith(".gif")))
                    	throw new Exception("Invalid file format. Only images are allowed. ");
                    
                    byte[] bytes = file.getBytes();
                    
                    Random rn = new Random();
                    
                    String fileNameArray[] = fileName.split("\\.");
                    String fileType = fileNameArray[fileNameArray.length-1];
                    
                    String newFileName = String.valueOf(rn.nextInt(9000001) + 1000000) + String.valueOf(new Date().getTime()) + "." + fileType.toLowerCase();
                    
                    File systemFile = new File("/root/tomcat7/webapps/images/" + newFileName);
                    
                    //Throw an error if the file name already exists on the server. This will ensure no file is overwritten by someone else
                    if(systemFile.exists())
                    	throw new Exception("A file with that name already exists. Please rename the file and try again. ");
                    
                    //Create the new file on the sever
                    BufferedOutputStream buffStream = 
                            new BufferedOutputStream(new FileOutputStream(systemFile));
                    buffStream.write(bytes);
                    buffStream.close();
                    //Success message back to the JSP page
                    json.setSuccessMessage("You have successfully uploaded " + fileName);
                    json.setImageName(newFileName);
                } catch (Exception e) {
                	//errorMessage back to the JSP page. Show the thrown message text as the error detail
                	json.setErrorMessage("There was a problem uploading your file: " + e.getMessage() + " " + fileName);
                }
            } else {
                json.setErrorMessage("Invalid file, please try another file.");
            }
       	}
       	return json; 
    }
    
    /* (non-Javadoc)
     * This is the entry point for submitting the form data from the uploadImages.jsp page (POST request)
     */
    @RequestMapping(value = "/v1/UpdateAddress", method = RequestMethod.GET)  
   	public @ResponseBody JSONInsertResponse updateAddress(
   			@RequestParam("lat") String lat,
   			@RequestParam("lon") String lon,
   			@RequestParam("addressLine1") String addressLine1,
   			@RequestParam(value="addressLine2", required=false) String addressLine2,
   			@RequestParam("city") String city,
   			@RequestParam("state") String state,
   			@RequestParam("zip") String zip,
   			@RequestParam(value="country", required=false) String country,
   			@RequestParam(value="apiKey", required=false) String apiKey
   			) {  
       	
    	//Check if this attempt is authorized
    	Map<String, String> userDetails = authorize(apiKey);
       	JSONInsertResponse json = new JSONInsertResponse();
       	if(userDetails == null || userDetails.size() < 2){
       		//unauthorized
       		json.setErrorMessage(UNAUTHENTICATED);
       	}else{
       		//authorized
       		List<Address> addresses = (List<Address>) dao.getAddress(addressLine1, addressLine2, city, state, zip, country);
       		if(addresses != null && addresses.size() > 0){
       			for(Address address : addresses){
	       			if(address != null){
			       		address.setLat(lat);
			       		address.setLon(lon);
			       		dao.updateAddress(address);
			       		json.setSuccessMessage("Address successfully updated");
	       			}
       			}
       		}
       	}
       	return json; 
    }
    
    @RequestMapping(value = "/v1/SaveOrUpdateWebhooks", method = RequestMethod.POST)  
   	public @ResponseBody JSONInsertResponse saveOrUpdateWebhooks(
   			@RequestParam("url") String url,
   			@RequestParam(value="apiKey", required=false) String apiKey
   			) {  
       	
    	//Check if this attempt is authorized
       	Map<String, String> userDetails = authorize(apiKey);
       	JSONInsertResponse json = new JSONInsertResponse();
       	if(userDetails == null || userDetails.size() < 2){
       		//unauthorized
       		json.setErrorMessage(UNAUTHENTICATED);
       	}else{
       		//authorized
       		List<Webhooks> webhooks = (List<Webhooks>) dao.getWebhooksByShopCode(userDetails.get("userName"));
       		if(webhooks != null && webhooks.size() > 0){
       			Webhooks foundWebhooks = webhooks.get(0);
       			foundWebhooks.setUrl(url);
       			dao.updateWebhooks(foundWebhooks);
       		}else{
       			Webhooks newWebhooks = new Webhooks();
       			newWebhooks.setShopCode(userDetails.get(1));
       			newWebhooks.setUrl(url);
       			dao.addWebhooks(newWebhooks);
       		}
       		json.setSuccessMessage("Webhooks registered succussfully");
       	}
       	return json; 
    }
    
    private void initiateWebhooks(final String shopCode, final Delivery delivery, final String apiKey){
    	Thread thread = new Thread(new Runnable() {
            public void run() {
            	List<Webhooks> webhooks = (List<Webhooks>) dao.getWebhooksByShopCode(shopCode);
           		if(webhooks != null && webhooks.size() > 0){
           			
           			Webhooks foundWebhooks = webhooks.get(0);
           			String url = foundWebhooks.getUrl();
           			
           			MyHttpClient client = new MyHttpClient();
           			CloseableHttpClient httpclient = null;
			
					try {
						httpclient = client.getNewHttpClient(url);
	
		            	HttpPost httppost = new HttpPost("https://" + url);
		            	ObjectMapper mapper = new ObjectMapper();
		            	mapper.setSerializationInclusion(Include.NON_NULL);
		            	
		            	String deliveryImageURL = delivery.getDeliveryImageURL();
    				    if(deliveryImageURL != null && !deliveryImageURL.equals("") ){
    					  URL url2 = new URL(deliveryImageURL);
    				      InputStream in = url2.openStream();
    					  String text = Base64Utils.encodeToString(IOUtils.toByteArray(in));
    					  delivery.setDeliveryImageBase64(text);
    					  delivery.setDeliveryImageURL("true");
    				    }
    				    
    				    String signatureImageURL = delivery.getSignatureImageURL();
    				    if(signatureImageURL != null && !signatureImageURL.equals("") ){
    					  URL url2 = new URL(signatureImageURL);
    				      InputStream in = url2.openStream();
    					  String text = Base64Utils.encodeToString(IOUtils.toByteArray(in));
    					  delivery.setSignatureImageBase64(text);
    					  delivery.setSignatureImageURL("true");
    				    }
    				    
		            	String json = mapper.writeValueAsString(delivery);
		            	HttpEntity httpEnt = null;
		            	
		            	if(usersList.contains(shopCode)){
		            		
		            		HttpPost httppost2 = new HttpPost("https://fast-api.800-flowers.net/r/api/session/guesttoken");
		        		    httppost2.addHeader("Content-Type","application/json");
		        		    httppost2.addHeader("Cookie","GCLB=CJrewOfXu8eazAE");
		        		    StringEntity params = new StringEntity("{\"guid\":\"TI5XeNnbEpfGa26i6lTK5YQw71GnjjB0 M6HxPpjqvZpTdZGtkwY9p0ceoKpDTU1tje0I8h7_CvB9QdP6BXiPKw1qyIc0a3SC\"}");
		        		    httppost2.setEntity(params);
		        		    
		        		    HttpResponse response = httpclient.execute(httppost2);
		        		    HttpEntity resEntity = response.getEntity();
		        		    String token = EntityUtils.toString(resEntity).split("accessToken\":\"")[1].split("\"")[0];
		        		    EntityUtils.consume(resEntity);
		        		    
		        		    params = new StringEntity("{\"deliveryJSON\":"+json+"}");
		        		    
		        		    httppost.setEntity(params);
		        		    httppost.addHeader("Authorization","Bearer " + token);
		        		    httppost.addHeader("Content-Type","application/json");

		            	}else{
		            		httpEnt = MultipartEntityBuilder.create()
			            			.addPart("deliveryJSON", new StringBody(json, ContentType.TEXT_PLAIN))
			            			.addPart("apiKey", new StringBody(apiKey, ContentType.TEXT_PLAIN))
						    		.build();
		            		httppost.setEntity(httpEnt);
		            	}
		            	
		        	    
		        	    HttpResponse response = httpclient.execute(httppost);
						HttpEntity resEntity = response.getEntity();
						EntityUtils.consume(resEntity);
						return;
						
					}catch(Exception ee){
						ee.printStackTrace();
						return;
					}finally{
						try {
							httpclient.close();
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
           		}
           		return;
            }
    	});
    	thread.start();
    }
    
    /* @RequestMapping(value = "/v1/RetrieveDeliveriesByDeliveryImagesAndCreatedDate", method = RequestMethod.GET)  
  	public @ResponseBody List<JSONRetrievalResponse> retrieveDeliveriesByDeliveryImagesAndCreatedDate(
  			@RequestParam(value="apiKey", required=false) String apiKey,
  			@RequestParam(value="startDate", required=false) String startDate,
  			@RequestParam(value="endDate", required=false) String endDate
  			) {
      	List<JSONRetrievalResponse> results = new ArrayList<JSONRetrievalResponse>();
      	JSONRetrievalResponse json = new JSONRetrievalResponse();
      	//Check to see if this is an authenticated attempt
      	String userDetails = authorize(apiKey);
      	
      	if(userDetails != null && userDetails.size() == 2){
      		//authenticated
      		
      		if(startDate == null || startDate.trim().equals("")){
      			json.setErrorMessage("A start date was not specified. Please try again after correcting your request.");
      			results.add(json);
      			return results;
      		}
      		
      		if(endDate == null || endDate.trim().equals("")){
      			json.setErrorMessage("An end date was not specified. Please try again after correcting your request.");
      			results.add(json);
      			return results;
      		}
      		
      		//get the delivery data from the database, as a list
      		List<Delivery> deliveries = dao.getDeliveriesByDeliveryImageAndCreatedDate(startDate,endDate);
      		if(deliveries != null && deliveries.size() > 0){
      			
      			for(int ii = 0; ii < deliveries.size(); ++ ii){
  	    			Delivery delivery = deliveries.get(ii);
  	    			json = createJSON(delivery);	    			
  	    			results.add(json);
      			}
      			
      		}else{
      			json.setErrorMessage("No deliveries were found with delivery images for the provided created date range");
      			results.add(json);
      			return results;
      		}
      		
      	}else{
      		//Unauthorized
      		json.setErrorMessage(UNAUTHENTICATED);
      		results.add(json);
  			return results;
      	}
      	
      	return results;  
      } */
    
    /* (non-Javadoc)
     * This method takes an apiKey, hashes & salts it, looks up the hashed value in the database,
     * and if a match is found it returns the userDetails, otherwise it returns null for later
     * authentication failure
     */
    public Map<String, String> authorize(String apiKey){

    	MessageDigest md = null;
		try {
			md = MessageDigest.getInstance("SHA-512");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		String salt = "h7M).hL9B6hjlScXz!a0Av$()77ScXzZ"; //hard coded here to prevent discovery in database or files system
		
		
		if(apiKey == null){
    		return null;
    	}else{
    		User user = null;
    		StringBuffer sb = new StringBuffer();
			md.update((apiKey+salt).getBytes());
			for (int xx=0; xx<10000; ++xx){
				 md.update(sb.toString().getBytes());
			     byte byteData[] = md.digest();
		        //convert the byte to hex format
		        sb = new StringBuffer();
		        for (int i = 0; i < byteData.length; i++) {
		        	sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
		        }
			}
	        List<User> users = dao.getUsersByAPIKey(sb.toString());
	        if(users != null && users.size() > 0){
	        	user = users.get(0);
	        }
			
			if(user != null){
				Map<String, String> userDetails = new HashMap<String, String>();
				userDetails.put("userID", user.getId());
				userDetails.put("userName", user.getUserName());
				return userDetails;
			}else{
				return null;
			}
    	}
	}
    
    private JSONRetrievalResponse createJSON(Delivery delivery){
    	
    	JSONRetrievalResponse json = new JSONRetrievalResponse();
    	json.setFulfiller(delivery.getFulfiller());
		json.setMerchant(delivery.getMerchant());
		json.setDeliveryService(delivery.getDeliveryService());
		json.setDeliveryCo(delivery.getDeliveryCo());
		json.setDriverName(delivery.getDriverName());
		json.setTargetDeliveryDate(delivery.getTargetDeliveryDate());
		json.setTargetDeliveryTime(delivery.getTargetDeliveryTime());
		json.setDeliveryDate(delivery.getDeliveryDate());
		json.setDeliveryTime(delivery.getDeliveryTime());
		json.setId(delivery.getId());
		json.setLeftAt(delivery.getLeftAt());
		json.setDeliveryNotes(delivery.getDeliveryNotes());
		json.setOrderNumberAtlas(delivery.getOrderNumberAtlas());
		json.setOrderNumberBLK(delivery.getOrderNumberBLK());
		json.setOrderNumberBMS(delivery.getOrderNumberBMS());
		json.setExternalId(delivery.getExternalId());
		json.setOrderNumberOther(delivery.getOrderNumberOther());
		json.setSignatureImageName(delivery.getSignatureImageName());
		json.setSignatureImageURL(delivery.getSignatureImageURL());
		json.setSignedBy(delivery.getSignedBy());
		json.setUserID(delivery.getUserID());
		json.setCreatedDate(delivery.getCreatedDate());
		json.setModifiedDate(delivery.getModifiedDate());
		json.setAddress(delivery.getAddress());
		json.setShopAddress(delivery.getShopAddress());
		json.setDeliveryImageURL(delivery.getDeliveryImageURL());
		json.setPreDeliveryImageURL(delivery.getPreDeliveryImageURL());
		json.setEstimatedDeliveryDate(delivery.getEstimatedDeliveryDate());
		json.setEstimatedDeliveryTime(delivery.getEstimatedDeliveryTime());
		json.setDeliveryCoTripID(delivery.getDeliveryCoTripID());
		json.setDeliveryCoID(delivery.getDeliveryCoID());
		json.setStatus(delivery.getStatus());
		json.setAttemptedReason(delivery.getAttemptedReason());
		json.setShopName(delivery.getShopName());
		
		return json;
    }
        
    private void loadProperties(){
    	
    	try {
			input = new FileInputStream("/var/bloomnet/properties/deliveryapi.properties");
			// load properties file
			properties.load(input);
		}catch (IOException ex) {
			ex.printStackTrace();
    	}
    }
}
