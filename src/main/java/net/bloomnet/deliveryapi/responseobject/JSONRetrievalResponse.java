package net.bloomnet.deliveryapi.responseobject;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import net.bloomnet.deliveryapi.entity.Delivery;

/* (non-Javadoc)
 * This is the response provided by an attempt to retrieve delivery data. This class
 * is automatically converted and provided to the end user as a JSON response by Jackson
 */
@JsonInclude(Include.NON_NULL)
public class JSONRetrievalResponse extends Delivery {
	
	private String errorMessage;

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
}
