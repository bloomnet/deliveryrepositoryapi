package test;
import java.io.File;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

@SuppressWarnings("deprecation")
public class PostDeliveryData{
	
	private static final String apiKey = "dg(JfENFdfYtb7*aaKe18cXyY)0Bq";

	  public static void main(String[] args) throws Exception {
		///////////////////Save an image for use with data saving later on  
		MyHttpClient client = new MyHttpClient();//use this client to test production
	    @SuppressWarnings("resource")
		HttpClient client2 = new DefaultHttpClient();//use this client to test locally
	    
		CloseableHttpClient httpclient = client.getNewHttpClient(false);//Staging use ONLY, accepts ALL certifcates
		//CloseableHttpClient httpclient = client.getNewHttpClient(true);//Production use

	   HttpPost httppost = new HttpPost("https://delivery.bloomnet.net/DeliveryAPI/v1/SaveImage");
	    File file = new File("C:\\102423.png");
	    
	    HttpEntity httpEnt = MultipartEntityBuilder.create()
	    		.addPart("apiKey", new StringBody(apiKey, ContentType.TEXT_PLAIN))
	    		.addPart("file", new FileBody(file, ContentType.MULTIPART_FORM_DATA))
	    		.build();
	    httppost.setEntity(httpEnt);
	    
	    System.out.println("executing request " + httppost.getRequestLine());
	    
	    HttpResponse response = httpclient.execute(httppost);
	    HttpEntity resEntity = response.getEntity();

	    String savedImage = "";
	    if (resEntity != null) {
	      String JSON = EntityUtils.toString(resEntity);
	      
	      if(JSON.contains("successMessage")){
	    	  savedImage = JSON.split("\"imageName\":\"")[1].split("\"")[0];
	      }else{
	    	  savedImage = "no image saved";
	      }
	      System.out.println(JSON);
	      System.out.println(savedImage);
	      EntityUtils.consume(resEntity);
	    }
	    //////////////////Save delivery Data
		httppost = new HttpPost("http://localhost:8080/DeliveryAPI/v1/InsertOrUpdateDeliveryInfo");
		    httpEnt = MultipartEntityBuilder.create()
		    		.addPart("apiKey", new StringBody(apiKey, ContentType.TEXT_PLAIN))
		    		.addPart("fulfiller", new StringBody("A2220000", ContentType.TEXT_PLAIN))
		    		.addPart("deliveryDate", new StringBody("09/27/2020", ContentType.TEXT_PLAIN))
		    		.addPart("orderNumberBMS", new StringBody("SPIDERMAN-02760115525556h7582", ContentType.TEXT_PLAIN))
		    		.addPart("deliveryImageURL", new StringBody("http://adb6def9928467589ebb-f540d5a8d53c2e76ad581b6e5c346ad6.r74.cf1.rackcdn.com/f140776feaf36b419a1a1975b5a302c8.jpg", ContentType.TEXT_PLAIN))
		    		.addPart("addressLine1", new StringBody("2041-22 JAFFREYYT ST", ContentType.TEXT_PLAIN))
		    		.addPart("city", new StringBody("WEYMOUTH", ContentType.TEXT_PLAIN))
		    		.addPart("state", new StringBody("MA", ContentType.TEXT_PLAIN))
		    		.addPart("zip", new StringBody("02188", ContentType.TEXT_PLAIN))
		    		.addPart("country", new StringBody("US", ContentType.TEXT_PLAIN))
		    		.addPart("shopAddressLine1", new StringBody("39 Walnut St", ContentType.TEXT_PLAIN))
		    		.addPart("shopCity", new StringBody("WEYMOUTH", ContentType.TEXT_PLAIN))
		    		.addPart("shopState", new StringBody("MA", ContentType.TEXT_PLAIN))
		    		.addPart("shopZip", new StringBody("02188", ContentType.TEXT_PLAIN))
		    		.addPart("shopCountry", new StringBody("US", ContentType.TEXT_PLAIN))
		    		.addPart("targetDeliveryDate", new StringBody("09/27/2020", ContentType.TEXT_PLAIN))
		    		.addPart("leftAt", new StringBody("Side Door", ContentType.TEXT_PLAIN))
		    		.addPart("merchant", new StringBody("00800000", ContentType.TEXT_PLAIN))
		    		.addPart("preDeliveryImageURL", new StringBody("", ContentType.TEXT_PLAIN))
		    		.addPart("signatureImageURL", new StringBody("", ContentType.TEXT_PLAIN))
		    		.addPart("status", new StringBody("Delivered", ContentType.TEXT_PLAIN))
		    		.addPart("attemptedReason", new StringBody("Could Not Find Address", ContentType.TEXT_PLAIN))
		    		.addPart("shopName", new StringBody("Mark's Test Shop", ContentType.TEXT_PLAIN))
		    		.addPart("lat", new StringBody("46.244700", ContentType.TEXT_PLAIN))
		    		.addPart("lon", new StringBody("-105.267500", ContentType.TEXT_PLAIN))
		    		.build();
		    httppost.setEntity(httpEnt);
		    System.out.println("executing request " + httppost.getRequestLine());
		    long start = System.currentTimeMillis();
		    response = client2.execute(httppost);
		    long end = System.currentTimeMillis();
		    System.out.println("Execution Time: " + String.valueOf(end-start-100) + "ms.");
		    resEntity = response.getEntity();
		    System.out.println(EntityUtils.toString(resEntity));
		    EntityUtils.consume(resEntity);  
		    
		    /*httppost = new HttpPost("https://delivery.bloomnet.net/DeliveryAPI/v1/SaveOrUpdateWebhooks");
		    httpEnt = MultipartEntityBuilder.create()
		    		.addPart("apiKey", new StringBody(apiKey, ContentType.TEXT_PLAIN))
		    		.addPart("shopCode", new StringBody("00800000", ContentType.TEXT_PLAIN))
		    		.addPart("url", new StringBody("fast-api.800-flowers.net/r/api/retention/selfservice/poc/SaveOrUpdateDeliveryHubOrder", ContentType.TEXT_PLAIN))
		    		.build();
		    httppost.setEntity(httpEnt);

		    System.out.println("executing request " + httppost.getRequestLine());
		    start = System.currentTimeMillis();
		    response = httpclient.execute(httppost);
		    end = System.currentTimeMillis();
		    System.out.println("Execution Time: " + String.valueOf(end-start) + "ms.");
		    resEntity = response.getEntity();
		    System.out.println(EntityUtils.toString(resEntity));
		    EntityUtils.consume(resEntity);*/
		    
		
		    for(int ii=0; ii<0; ii++){
			    HttpPost httppost2 = new HttpPost("https://fast-api.800-flowers.net/r/api/session/guesttoken");
			    httppost2.addHeader("Content-Type","application/json");
			    httppost2.addHeader("Cookie","GCLB=CJrewOfXu8eazAE");
			    StringEntity params = new StringEntity("{\"guid\":\"TI5XeNnbEpfGa26i6lTK5YQw71GnjjB0 M6HxPpjqvZpTdZGtkwY9p0ceoKpDTU1tje0I8h7_CvB9QdP6BXiPKw1qyIc0a3SC\"}");
			    httppost2.setEntity(params);
			    
			    response = httpclient.execute(httppost2);
			    resEntity = response.getEntity();
			    String token = EntityUtils.toString(resEntity).split("accessToken\":\"")[1].split("\"")[0];
			    System.out.println(token);
			    EntityUtils.consume(resEntity);
		    }
		    
		    /*HttpPost httppost = new HttpPost("https://fast-api.800-flowers.net/r/api/retention/selfservice/poc/SaveOrUpdateDeliveryHubOrder");
		    Delivery delivery = new Delivery();
		    delivery.setDeliveryNotes("This was submitted correctly");
		    delivery.setStatus("DeliveryAttempted");
		    delivery.setAttemptedReason("Could not find house");
		    delivery.setCreatedDate("12/02/2020 00:00:00");
		    delivery.setDeliveryDate("12/03/2020");
		    delivery.setDeliveryTime("12:00AM");
		    delivery.setFulfiller("M1770000");
		    delivery.setOrderNumberBMS("TESTOrder1234567");
		    delivery.setOrderNumberAtlas("TESTOrder12345678");
		    delivery.setMerchant("00800000");
		    delivery.setTargetDeliveryDate("12/03/2020");
		    delivery.setLeftAt("Side Door");
		    Address recipientAddress = new Address();
		    recipientAddress.setAddressLine1("123 Test St.");
		    recipientAddress.setCity("New York");
		    recipientAddress.setState("NY");
		    recipientAddress.setCountry("USA");
		    recipientAddress.setZip("10010");
		    delivery.setAddress(recipientAddress);
		    Address shopAddress = new Address();
		    shopAddress.setAddressLine1("123 Test St.");
		    shopAddress.setCity("New York");
		    shopAddress.setState("NY");
		    shopAddress.setCountry("USA");
		    shopAddress.setZip("10010");
		    delivery.setShopAddress(shopAddress);
		    ObjectMapper mapper = new ObjectMapper();
		    delivery.setShopName("Mark Shop Five");
		    mapper.setSerializationInclusion(Include.NON_NULL);
		    String json = mapper.writeValueAsString(delivery);
		    System.out.println("{\"deliveryJSON\":"+json+",\"\"}");
		    
		    params = new StringEntity("{\"deliveryJSON\":"+json+"}");
		    
		    httppost.setEntity(params);
		    httppost.addHeader("Authorization","Bearer " + token);
		    httppost.addHeader("Content-Type","application/json");
		    System.out.println("executing request " + httppost.getRequestLine());
		    response = httpclient.execute(httppost);
		    resEntity = response.getEntity();
		    System.out.println("response: " + EntityUtils.toString(resEntity));
		    EntityUtils.consume(resEntity);*/
		   	   
		    
		   httppost = new HttpPost("https://delivery.bloomnet.net/DeliveryAPI/v1/InsertOrUpdateDeliveryInfoInBulk");
		     String jsonString = "[ {\"address\":{\"addressLine1\":\"6 Bayview Ave.\",\"addressLine2\":\"Suite 1\",\"city\":\"Northport\",\"state\":\"NY\",\"zip\":\"11768\",\"country\":\"US\"}, \"shopAddress\" : {\"addressLine1\" : \"39 Walnut St\", \"city\" : \"West Hempstead\", \"state\" : \"NY\", \"zip\" : \"11552\", \"country\" : \"USA\"}, \"fulfiller\":\"A2220000\",\"merchant\":\"08000000\",\"deliveryDate\":\"01/21/2019\",\"targetDeliveryDate\":\"\",\"targetDeliveryTime\":\"\",\"deliveryTime\":\"3PM\",\"orderNumberBMS\":\"9105972_DJD\",\"externalId\":\"xyz\",\"deliveryCo\":\"Onfleet\",\"leftAt\":\"Back door\",\"signedBy\":\"Billy-Bob\",\"signatureImageURL\":\"http://originalflash.com/images/signature_scan.gif\",\"deliveryImageURL\":\"http://www.flower-delivery-online.net/delivery.jpg\",\"driverName\":\"Dave D'Amico\",\"deliveryService\":\"Same day\",\"deliveryNotes\":\"leave at the back door\",\"estimatedDeliveryDate\":\"01/22/2020\",\"estimatedDeliveryTime\":\"03:57 PM\",\"deliveryCoTripID\":\"12345\",\"deliveryCoID\":\"xyz\",\"status\":\"DeliveryAttempted\"},"
		       + "{\"fulfiller\" : \"B5230000\", \"deliveryDate\" : \"06/05/2017\", \"orderNumberBMS\" : \"SPIDERMAN28280\", \"deliveryTime\" : \"12:00PM\", \"leftAt\" : \"Side Door\", \"signedBy\" : \"Mark Silver\", \"address\" : {\"addressLine1\" : \"39 Walnut St\", \"city\" : \"West Hempstead\", \"state\" : \"NY\", \"zip\" : \"11552\", \"country\" : \"USA\"}, \"shopAddress\" : {\"addressLine1\" : \"39 Walnut St\", \"city\" : \"West Hempstead\", \"state\" : \"NY\", \"zip\" : \"11552\", \"country\" : \"USA\"}, \"shopName\" : \"Mark's Shop\", \"targetDeliveryDate\" : \"06/05/2017\", \"targetDeliveryTime\" : \"12:00PM\", \"driverName\" : \"Pablo\", \"merchant\" : \"M1770000\", \"deliveryNotes\" : \"Done Good!\"},"
		       + "{\"fulfiller\" : \"B5230000\", \"deliveryDate\" : \"06/05/2017\", \"orderNumberBMS\" : \"SPIDERMAN300\", \"deliveryTime\" : \"12:00PM\", \"leftAt\" : \"Side Door\", \"signedBy\" : \"Mark Silver\", \"address\" : {\"addressLine1\" : \"39 Walnut St\", \"city\" : \"West Hempstead\", \"state\" : \"NY\", \"zip\" : \"11552\", \"country\" : \"USA\"}, \"shopAddress\" : {\"addressLine1\" : \"39 Walnut St\", \"city\" : \"West Hempstead\", \"state\" : \"NY\", \"zip\" : \"11552\", \"country\" : \"USA\"}, \"shopName\" : \"Mark's Shop\", \"targetDeliveryDate\" : \"06/05/2017\", \"targetDeliveryTime\" : \"12:00PM\", \"driverName\" : \"Pablo\", \"merchant\" : \"M1770000\", \"deliveryNotes\" : \"Done Good!\"},"
		       + "{\"fulfiller\" : \"B5230000\", \"deliveryDate\" : \"06/05/2017\", \"orderNumberBMS\" : \"SPIDERMAN4980\", \"deliveryTime\" : \"12:00PM\", \"leftAt\" : \"Side Door\", \"signedBy\" : \"Mark Silver\", \"address\" : {\"addressLine1\" : \"39 Walnut St\", \"city\" : \"West Hempstead\", \"state\" : \"NY\", \"zip\" : \"11552\", \"country\" : \"USA\"}, \"shopAddress\" : {\"addressLine1\" : \"39 Walnut St\", \"city\" : \"West Hempstead\", \"state\" : \"NY\", \"zip\" : \"11552\", \"country\" : \"USA\"}, \"shopName\" : \"Mark's Shop\", \"targetDeliveryDate\" : \"06/05/2017\", \"targetDeliveryTime\" : \"12:00PM\", \"driverName\" : \"Pablo\", \"merchant\" : \"M1770000\", \"deliveryNotes\" : \"Done Good!\"},"
		       + "{\"fulfiller\" : \"B5230000\", \"deliveryDate\" : \"06/05/2017\", \"orderNumberBMS\" : \"SPIDERMAN4990\", \"deliveryTime\" : \"12:00PM\", \"leftAt\" : \"Side Door\", \"signedBy\" : \"Mark Silver\", \"address\" : {\"addressLine1\" : \"39 Walnut St\", \"city\" : \"West Hempstead\", \"state\" : \"NY\", \"zip\" : \"11552\", \"country\" : \"USA\"}, \"shopAddress\" : {\"addressLine1\" : \"39 Walnut St\", \"city\" : \"West Hempstead\", \"state\" : \"NY\", \"zip\" : \"11552\", \"country\" : \"USA\"}, \"shopName\" : \"Mark's Shop\", \"targetDeliveryDate\" : \"06/05/2017\", \"targetDeliveryTime\" : \"12:00PM\", \"driverName\" : \"Pablo\", \"merchant\" : \"M1770000\", \"deliveryNotes\" : \"Done Good!\"},"
		       + "{\"fulfiller\" : \"B5230000\", \"deliveryDate\" : \"06/05/2017\", \"orderNumberBMS\" : \"SPIDERMAN4970\", \"deliveryTime\" : \"12:00PM\", \"leftAt\" : \"Side Door\", \"signedBy\" : \"Mark Silver\", \"address\" : {\"addressLine1\" : \"39 Walnut St\", \"city\" : \"West Hempstead\", \"state\" : \"NY\", \"zip\" : \"11552\", \"country\" : \"USA\"}, \"shopAddress\" : {\"addressLine1\" : \"39 Walnut St\", \"city\" : \"West Hempstead\", \"state\" : \"NY\", \"zip\" : \"11552\", \"country\" : \"USA\"}, \"shopName\" : \"Mark's Shop\", \"targetDeliveryDate\" : \"06/05/2017\", \"targetDeliveryTime\" : \"12:00PM\", \"driverName\" : \"Pablo\", \"merchant\" : \"M1770000\", \"deliveryNotes\" : \"Done Good!\"},"
		       + "{\"fulfiller\" : \"B5230000\", \"deliveryDate\" : \"06/05/2017\", \"orderNumberBMS\" : \"SPIDERMAN4960\", \"deliveryTime\" : \"12:00PM\", \"leftAt\" : \"Side Door\", \"signedBy\" : \"Mark Silver\", \"address\" : {\"addressLine1\" : \"39 Walnut St\", \"city\" : \"West Hempstead\", \"state\" : \"NY\", \"zip\" : \"11552\", \"country\" : \"USA\"}, \"shopAddress\" : {\"addressLine1\" : \"39 Walnut St\", \"city\" : \"West Hempstead\", \"state\" : \"NY\", \"zip\" : \"11552\", \"country\" : \"USA\"}, \"shopName\" : \"Mark's Shop\", \"targetDeliveryDate\" : \"06/05/2017\", \"targetDeliveryTime\" : \"12:00PM\", \"driverName\" : \"Pablo\", \"merchant\" : \"M1770000\", \"deliveryNotes\" : \"Done Good!\"},"
		       + "{\"fulfiller\" : \"B5230000\", \"deliveryDate\" : \"06/05/2017\", \"orderNumberBMS\" : \"SPIDERMAN4950\", \"deliveryTime\" : \"12:00PM\", \"leftAt\" : \"Side Door\", \"signedBy\" : \"Mark Silver\", \"address\" : {\"addressLine1\" : \"39 Walnut St\", \"city\" : \"West Hempstead\", \"state\" : \"NY\", \"zip\" : \"11552\", \"country\" : \"USA\"}, \"shopAddress\" : {\"addressLine1\" : \"39 Walnut St\", \"city\" : \"West Hempstead\", \"state\" : \"NY\", \"zip\" : \"11552\", \"country\" : \"USA\"}, \"shopName\" : \"Mark's Shop\", \"targetDeliveryDate\" : \"06/05/2017\", \"targetDeliveryTime\" : \"12:00PM\", \"driverName\" : \"Pablo\", \"merchant\" : \"M1770000\", \"deliveryNotes\" : \"Done Good!\"},"
		       + "{\"fulfiller\" : \"B5230000\", \"deliveryDate\" : \"06/05/2017\", \"orderNumberBMS\" : \"SPIDERMAN4940\", \"deliveryTime\" : \"12:00PM\", \"leftAt\" : \"Side Door\", \"signedBy\" : \"Mark Silver\", \"address\" : {\"addressLine1\" : \"39 Walnut St\", \"city\" : \"West Hempstead\", \"state\" : \"NY\", \"zip\" : \"11552\", \"country\" : \"USA\"}, \"shopAddress\" : {\"addressLine1\" : \"39 Walnut St\", \"city\" : \"West Hempstead\", \"state\" : \"NY\", \"zip\" : \"11552\", \"country\" : \"USA\"}, \"shopName\" : \"Mark's Shop\", \"targetDeliveryDate\" : \"06/05/2017\", \"targetDeliveryTime\" : \"12:00PM\", \"driverName\" : \"Pablo\", \"merchant\" : \"M1770000\", \"deliveryNotes\" : \"Done Good!\"},"
		       + "{\"fulfiller\" : \"B5230000\", \"deliveryDate\" : \"06/05/2017\", \"orderNumberBMS\" : \"SPIDERMAN4930\", \"deliveryTime\" : \"12:00PM\", \"leftAt\" : \"Side Door\", \"signedBy\" : \"Mark Silver\", \"address\" : {\"addressLine1\" : \"39 Walnut St\", \"city\" : \"West Hempstead\", \"state\" : \"NY\", \"zip\" : \"11552\", \"country\" : \"USA\"}, \"shopAddress\" : {\"addressLine1\" : \"39 Walnut St\", \"city\" : \"West Hempstead\", \"state\" : \"NY\", \"zip\" : \"11552\", \"country\" : \"USA\"}, \"shopName\" : \"Mark's Shop\", \"targetDeliveryDate\" : \"06/05/2017\", \"targetDeliveryTime\" : \"12:00PM\", \"driverName\" : \"Pablo\", \"merchant\" : \"M1770000\", \"deliveryNotes\" : \"Done Good!\"},"
		       + "{\"fulfiller\" : \"B5230000\", \"deliveryDate\" : \"06/05/2017\", \"orderNumberBMS\" : \"SPIDERMAN4920\", \"deliveryTime\" : \"12:00PM\", \"leftAt\" : \"Side Door\", \"signedBy\" : \"Mark Silver\", \"address\" : {\"addressLine1\" : \"39 Walnut St\", \"city\" : \"West Hempstead\", \"state\" : \"NY\", \"zip\" : \"11552\", \"country\" : \"USA\"}, \"shopAddress\" : {\"addressLine1\" : \"39 Walnut St\", \"city\" : \"West Hempstead\", \"state\" : \"NY\", \"zip\" : \"11552\", \"country\" : \"USA\"}, \"shopName\" : \"Mark's Shop\", \"targetDeliveryDate\" : \"06/05/2017\", \"targetDeliveryTime\" : \"12:00PM\", \"driverName\" : \"Pablo\", \"merchant\" : \"M1770000\", \"deliveryNotes\" : \"Done Good!\"},"
		       + "{\"fulfiller\" : \"B5230000\", \"deliveryDate\" : \"06/05/2017\", \"orderNumberBMS\" : \"SPIDERMAN4910\", \"deliveryTime\" : \"12:00PM\", \"leftAt\" : \"Side Door\", \"signedBy\" : \"Mark Silver\", \"address\" : {\"addressLine1\" : \"39 Walnut St\", \"city\" : \"West Hempstead\", \"state\" : \"NY\", \"zip\" : \"11552\", \"country\" : \"USA\"}, \"shopAddress\" : {\"addressLine1\" : \"39 Walnut St\", \"city\" : \"West Hempstead\", \"state\" : \"NY\", \"zip\" : \"11552\", \"country\" : \"USA\"}, \"shopName\" : \"Mark's Shop\", \"targetDeliveryDate\" : \"06/05/2017\", \"targetDeliveryTime\" : \"12:00PM\", \"driverName\" : \"Pablo\", \"merchant\" : \"M1770000\", \"deliveryNotes\" : \"Done Good!\"},"
		       + "{\"fulfiller\" : \"B5230000\", \"deliveryDate\" : \"06/05/2017\", \"orderNumberBMS\" : \"SPIDERMAN4900\", \"deliveryTime\" : \"12:00PM\", \"leftAt\" : \"Side Door\", \"signedBy\" : \"Mark Silver\", \"address\" : {\"addressLine1\" : \"39 Walnut St\", \"city\" : \"West Hempstead\", \"state\" : \"NY\", \"zip\" : \"11552\", \"country\" : \"USA\"}, \"shopName\" : \"Mark's Shop\", \"targetDeliveryDate\" : \"06/05/2017\", \"targetDeliveryTime\" : \"12:00PM\", \"driverName\" : \"Pablo\", \"merchant\" : \"M1770000\", \"deliveryNotes\" : \"Done Good!\"},"
		       + "{\"fulfiller\" : \"B5230000\", \"deliveryDate\" : \"06/05/2017\", \"orderNumberBMS\" : \"SPIDERMAN4890\", \"deliveryTime\" : \"12:00PM\", \"leftAt\" : \"Side Door\", \"signedBy\" : \"Mark Silver\", \"address\" : {\"addressLine1\" : \"39 Walnut St\", \"city\" : \"West Hempstead\", \"state\" : \"NY\", \"zip\" : \"11552\", \"country\" : \"USA\"}, \"shopAddress\" : {\"addressLine1\" : \"39 Walnut St\", \"city\" : \"West Hempstead\", \"state\" : \"NY\", \"zip\" : \"11552\", \"country\" : \"USA\"}, \"shopName\" : \"Mark's Shop\", \"targetDeliveryDate\" : \"06/05/2017\", \"targetDeliveryTime\" : \"12:00PM\", \"driverName\" : \"Pablo\", \"merchant\" : \"M1770000\", \"deliveryNotes\" : \"Done Good!\"},"
		       + "{\"fulfiller\" : \"B5230000\", \"deliveryDate\" : \"06/05/2017\", \"orderNumberBMS\" : \"SPIDERMAN4880\", \"deliveryTime\" : \"12:00PM\", \"leftAt\" : \"Side Door\", \"signedBy\" : \"Mark Silver\", \"address\" : {\"addressLine1\" : \"39 Walnut St\", \"city\" : \"West Hempstead\", \"state\" : \"NY\", \"zip\" : \"11552\", \"country\" : \"USA\"}, \"shopAddress\" : {\"addressLine1\" : \"39 Walnut St\", \"city\" : \"West Hempstead\", \"state\" : \"NY\", \"zip\" : \"11552\", \"country\" : \"USA\"}, \"shopName\" : \"Mark's Shop\", \"targetDeliveryDate\" : \"06/05/2017\", \"targetDeliveryTime\" : \"12:00PM\", \"driverName\" : \"Pablo\", \"merchant\" : \"M1770000\", \"deliveryNotes\" : \"Done Good!\"},"
		       + "{\"fulfiller\" : \"B5230000\", \"deliveryDate\" : \"06/05/2017\", \"orderNumberBMS\" : \"SPIDERMAN4870\", \"deliveryTime\" : \"12:00PM\", \"leftAt\" : \"Side Door\", \"signedBy\" : \"Mark Silver\", \"address\" : {\"addressLine1\" : \"39 Walnut St\", \"city\" : \"West Hempstead\", \"state\" : \"NY\", \"zip\" : \"11552\", \"country\" : \"USA\"}, \"shopAddress\" : {\"addressLine1\" : \"39 Walnut St\", \"city\" : \"West Hempstead\", \"state\" : \"NY\", \"zip\" : \"11552\", \"country\" : \"USA\"}, \"shopName\" : \"Mark's Shop\", \"targetDeliveryDate\" : \"06/05/2017\", \"targetDeliveryTime\" : \"12:00PM\", \"driverName\" : \"Pablo\", \"merchant\" : \"M1770000\", \"deliveryNotes\" : \"Done Good!\"},"
		       + "{\"fulfiller\" : \"B5230000\", \"deliveryDate\" : \"06/05/2017\", \"orderNumberBMS\" : \"SPIDERMAN4860\", \"deliveryTime\" : \"12:00PM\", \"leftAt\" : \"Side Door\", \"signedBy\" : \"Mark Silver\", \"address\" : {\"addressLine1\" : \"39 Walnut St\", \"city\" : \"West Hempstead\", \"state\" : \"NY\", \"zip\" : \"11552\", \"country\" : \"USA\"}, \"shopAddress\" : {\"addressLine1\" : \"39 Walnut St\", \"city\" : \"West Hempstead\", \"state\" : \"NY\", \"zip\" : \"11552\", \"country\" : \"USA\"}, \"shopName\" : \"Mark's Shop\", \"targetDeliveryDate\" : \"06/05/2017\", \"targetDeliveryTime\" : \"12:00PM\", \"driverName\" : \"Pablo\", \"merchant\" : \"M1770000\", \"deliveryNotes\" : \"Done Good!\"},"
		       + "{\"fulfiller\" : \"B5230000\", \"deliveryDate\" : \"06/05/2017\", \"orderNumberBMS\" : \"SPIDERMAN4850\", \"deliveryTime\" : \"12:00PM\", \"leftAt\" : \"Side Door\", \"signedBy\" : \"Mark Silver\", \"address\" : {\"addressLine1\" : \"39 Walnut St\", \"city\" : \"West Hempstead\", \"state\" : \"NY\", \"zip\" : \"11552\", \"country\" : \"USA\"}, \"shopAddress\" : {\"addressLine1\" : \"39 Walnut St\", \"city\" : \"West Hempstead\", \"state\" : \"NY\", \"zip\" : \"11552\", \"country\" : \"USA\"}, \"shopName\" : \"Mark's Shop\", \"targetDeliveryDate\" : \"06/05/2017\", \"targetDeliveryTime\" : \"12:00PM\", \"driverName\" : \"Pablo\", \"merchant\" : \"M1770000\", \"deliveryNotes\" : \"Done Good!\"},"
		       + "{\"fulfiller\" : \"B5230000\", \"deliveryDate\" : \"06/05/2017\", \"orderNumberBMS\" : \"SPIDERMAN4840\", \"deliveryTime\" : \"12:00PM\", \"leftAt\" : \"Side Door\", \"signedBy\" : \"Mark Silver\", \"address\" : {\"addressLine1\" : \"39 Walnut St\", \"city\" : \"West Hempstead\", \"state\" : \"NY\", \"zip\" : \"11552\", \"country\" : \"USA\"}, \"shopAddress\" : {\"addressLine1\" : \"39 Walnut St\", \"city\" : \"West Hempstead\", \"state\" : \"NY\", \"zip\" : \"11552\", \"country\" : \"USA\"}, \"shopName\" : \"Mark's Shop\", \"targetDeliveryDate\" : \"06/05/2017\", \"targetDeliveryTime\" : \"12:00PM\", \"driverName\" : \"Pablo\", \"merchant\" : \"M1770000\", \"deliveryNotes\" : \"Done Good!\"},"
		       + "{\"fulfiller\" : \"B5230000\", \"deliveryDate\" : \"06/05/2017\", \"orderNumberBMS\" : \"SPIDERMAN4830\", \"deliveryTime\" : \"12:00PM\", \"leftAt\" : \"Side Door\", \"signedBy\" : \"Mark Silver\", \"address\" : {\"addressLine1\" : \"39 Walnut St\", \"city\" : \"West Hempstead\", \"state\" : \"NY\", \"zip\" : \"11552\", \"country\" : \"USA\"}, \"shopAddress\" : {\"addressLine1\" : \"39 Walnut St\", \"city\" : \"West Hempstead\", \"state\" : \"NY\", \"zip\" : \"11552\", \"country\" : \"USA\"}, \"shopName\" : \"Mark's Shop\", \"targetDeliveryDate\" : \"06/05/2017\", \"targetDeliveryTime\" : \"12:00PM\", \"driverName\" : \"Pablo\", \"merchant\" : \"M1770000\", \"deliveryNotes\" : \"Done Good!\"},"
		       + "{\"fulfiller\" : \"B5230000\", \"deliveryDate\" : \"06/05/2017\", \"orderNumberBMS\" : \"SPIDERMAN4820\", \"deliveryTime\" : \"12:00PM\", \"leftAt\" : \"Side Door\", \"signedBy\" : \"Mark Silver\", \"address\" : {\"addressLine1\" : \"39 Walnut St\", \"city\" : \"West Hempstead\", \"state\" : \"NY\", \"zip\" : \"11552\", \"country\" : \"USA\"}, \"shopAddress\" : {\"addressLine1\" : \"39 Walnut St\", \"city\" : \"West Hempstead\", \"state\" : \"NY\", \"zip\" : \"11552\", \"country\" : \"USA\"}, \"shopName\" : \"Mark's Shop\", \"targetDeliveryDate\" : \"06/05/2017\", \"targetDeliveryTime\" : \"12:00PM\", \"driverName\" : \"Pablo\", \"merchant\" : \"M1770000\", \"deliveryNotes\" : \"Done Good!\"},"
		       + "{\"fulfiller\" : \"B5230000\", \"deliveryDate\" : \"06/05/2017\", \"orderNumberBMS\" : \"SPIDERMAN4810\", \"deliveryTime\" : \"12:00PM\", \"leftAt\" : \"Side Door\", \"signedBy\" : \"Mark Silver\", \"address\" : {\"addressLine1\" : \"39 Walnut St\", \"city\" : \"West Hempstead\", \"state\" : \"NY\", \"zip\" : \"11552\", \"country\" : \"USA\"}, \"shopAddress\" : {\"addressLine1\" : \"39 Walnut St\", \"city\" : \"West Hempstead\", \"state\" : \"NY\", \"zip\" : \"11552\", \"country\" : \"USA\"}, \"shopName\" : \"Mark's Shop\", \"targetDeliveryDate\" : \"06/05/2017\", \"targetDeliveryTime\" : \"12:00PM\", \"driverName\" : \"Pablo\", \"merchant\" : \"M1770000\", \"deliveryNotes\" : \"Done Good!\"},"
		       + "{\"fulfiller\" : \"B5230000\", \"deliveryDate\" : \"06/05/2017\", \"orderNumberBMS\" : \"SPIDERMAN4800\", \"deliveryTime\" : \"12:00PM\", \"leftAt\" : \"Side Door\", \"signedBy\" : \"Mark Silver\", \"address\" : {\"addressLine1\" : \"39 Walnut St\", \"city\" : \"West Hempstead\", \"state\" : \"NY\", \"zip\" : \"11552\", \"country\" : \"USA\"}, \"shopAddress\" : {\"addressLine1\" : \"39 Walnut St\", \"city\" : \"West Hempstead\", \"state\" : \"NY\", \"zip\" : \"11552\", \"country\" : \"USA\"}, \"shopName\" : \"Mark's Shop\", \"targetDeliveryDate\" : \"06/05/2017\", \"targetDeliveryTime\" : \"12:00PM\", \"driverName\" : \"Pablo\", \"merchant\" : \"M1770000\", \"deliveryNotes\" : \"Done Good!\"},"
		       + "{\"fulfiller\" : \"B5230000\", \"deliveryDate\" : \"06/05/2017\", \"orderNumberBMS\" : \"SPIDERMAN4790\", \"deliveryTime\" : \"12:00PM\", \"leftAt\" : \"Side Door\", \"signedBy\" : \"Mark Silver\", \"address\" : {\"addressLine1\" : \"39 Walnut St\", \"city\" : \"West Hempstead\", \"state\" : \"NY\", \"zip\" : \"11552\", \"country\" : \"USA\"}, \"shopAddress\" : {\"addressLine1\" : \"39 Walnut St\", \"city\" : \"West Hempstead\", \"state\" : \"NY\", \"zip\" : \"11552\", \"country\" : \"USA\"}, \"shopName\" : \"Mark's Shop\", \"targetDeliveryDate\" : \"06/05/2017\", \"targetDeliveryTime\" : \"12:00PM\", \"driverName\" : \"Pablo\", \"merchant\" : \"M1770000\", \"deliveryNotes\" : \"Done Good!\"},"
		       + "{\"fulfiller\" : \"B5230000\", \"deliveryDate\" : \"06/05/2017\", \"orderNumberBMS\" : \"SPIDERMAN29290\", \"deliveryTime\" : \"12:00PM\", \"leftAt\" : \"Side Door\", \"signedBy\" : \"Mark Silver\", \"address\" : {\"addressLine1\" : \"39 Walnut St\", \"city\" : \"West Hempstead\", \"state\" : \"NY\", \"zip\" : \"11552\", \"country\" : \"USA\"}, \"shopAddress\" : {\"addressLine1\" : \"39 Walnut St\", \"city\" : \"West Hempstead\", \"state\" : \"NY\", \"zip\" : \"11552\", \"country\" : \"USA\"}, \"shopName\" : \"Mark's Shop\", \"targetDeliveryDate\" : \"06/05/2017\", \"targetDeliveryTime\" : \"12:00PM\", \"driverName\" : \"Pablo\", \"merchant\" : \"M1770000\", \"deliveryNotes\" : \"Done Good!\"}]";

			    httpEnt = MultipartEntityBuilder.create()
			    		.addPart("apiKey", new StringBody("mf$Sf3m!tkdsZkf5nP(7dnsD$1co9DGjNsf2niCn$0vUf(I9em", ContentType.TEXT_PLAIN))
			    		.addPart("jsonArray", new StringBody(jsonString, ContentType.TEXT_PLAIN))
			    		.build();
			    httppost.setEntity(httpEnt); 
			    
			    System.out.println("executing request " + httppost.getRequestLine());
			    long timeStart = System.currentTimeMillis();
			    response = httpclient.execute(httppost);
			    long endTime = System.currentTimeMillis();
			    System.out.println("Completed in " + String.valueOf(endTime-timeStart) +"ms");
			    resEntity = response.getEntity();
			    System.out.println(EntityUtils.toString(resEntity));
			    EntityUtils.consume(resEntity); 
		
	    
	    ///////////////////Get Delivery Data
	    
	   HttpGet httpget = new HttpGet("https://delivery.bloomnet.net/DeliveryAPIBeta/v1/RetrieveDeliveryInfoByOrderNumber?apiKey="+apiKey+"&orderNumber=SPIDERMAN4860");
	    
	    
	    System.out.println("executing request " + httpget.getRequestLine());
	    start = System.currentTimeMillis();
	    response = httpclient.execute(httpget);
	    end = System.currentTimeMillis();
	    System.out.println("Execution Time: " + String.valueOf(end-start-100) + "ms.");
	    resEntity = response.getEntity();
	    System.out.println(EntityUtils.toString(resEntity));
	    EntityUtils.consume(resEntity);
	    
	    httpget = new HttpGet("https://delivery.bloomnet.net/DeliveryAPIBeta/v1/RetrieveDeliveryInfoHistoryByOrderNumber?apiKey="+apiKey+"&orderNumber=SPIDERMAN-02760115522");
	    
	    
	    System.out.println("executing request " + httpget.getRequestLine());
	    response = httpclient.execute(httpget);
	    resEntity = response.getEntity();
	    System.out.println(EntityUtils.toString(resEntity));
	    EntityUtils.consume(resEntity); 
	    
	    /*httpget = new HttpGet("https://delivery.bloomnet.net/DeliveryAPIBeta3/v1/RetrieveAddressesByDeliveryDate?apiKey="+apiKey+"&shopCode=A2220000&deliveryDate=07/27/2016");
	    
	    System.out.println("executing request " + httpget.getRequestLine());
	    response = httpclient.execute(httpget);
	    resEntity = response.getEntity();
	    System.out.println(EntityUtils.toString(resEntity));
	    EntityUtils.consume(resEntity);
	    httpclient.close();*/
	    
	  }
	  
}
